package control.task2;

public class Task2 {
    public static int sum = 0;

    public static void sum(int[] arr, int k) throws InterruptedException {
        int len = arr.length/k;
//        for (int i = 0; i < arr.length; i++) {
//            for (int j = i; j < i + len; i++) {
//                MyThread t = new MyThread(j, arr);
//            }
//        }
        for (int i = 0; i < k; i++) {
            if (i == k-1){
                Thread t = new MyThread(i*len + 1, arr.length - 1, arr);
                t.start();
                t.join();
            } else {
                Thread t = new MyThread(len * i + 1, (i+1) * len, arr);
                t.start();
                t.join();
            }
        }
    }



    public static void main(String[] args) throws InterruptedException {
        int[] arr = {1, 2, 3, 4, 5, 6};
        sum(arr, 1);
        System.out.println(sum);



    }
}
