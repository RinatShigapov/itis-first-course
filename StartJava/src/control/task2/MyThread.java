package control.task2;


public class MyThread extends Thread {
    private int start;
    private int end;
    private int[] array;
    private static final Object MUTEX = new Object();

    public MyThread(int start, int end, int[] array) {
        this.start = start;
        this.end = end;
        this.array = array;
    }

    @Override
    public void run() {
        for (int i = start; i <= end ; i++) {
            synchronized (MUTEX) {
            Task2.sum += array[i];
           }
        }
    }
}
