package control.task1;

import java.util.ArrayList;
import java.util.List;

public class Task1 {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        list.add("zabcdz");
        list.add("abababababab");
        list.add("abcbabababab");
        list.add("a");
        list.sort(((o1, o2) ->
        {        int max1 = 0;
            int cur = 0;
            char prev = (char) 0;
            for (int i = 0; i < o1.length(); i++) {
                if (o1.charAt(i) > prev) {
                    cur++;
                } else {
                    if (cur > max1) max1 = cur;
                    cur = 0;
                }
                prev = o1.charAt(i);
            }
            if (cur > max1) max1 = cur;

            int max2 = 0;
            int cur2 = 0;
            char prev2 = (char) 0;
            for (int i = 0; i < o2.length(); i++) {
                if (o2.charAt(i) > prev2) {
                    cur2++;
                } else {
                    if (cur2 > max2) max2 = cur2;
                    cur2 = 0;
                }
                prev2 = o2.charAt(i);
            }
            if (cur2 > max2) max2 = cur2;
            return max1 - max2;}));
        System.out.println(list);
    }
}
