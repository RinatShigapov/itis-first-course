package tasksAlgorithms;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner sc = new Scanner(System.in);
        int[] arr = new int[86400];
        int n = sc.nextInt();
        int elem, time;
        for (int i = 0; i < n; i++) {
            time = 0;
            for (int j = 0; j < 3; j++) {
                if (j == 2){
                    time += sc.nextInt();
                    break;
                }
                elem = sc.nextInt();
                time = (time + elem) * 60;
            }
            arr[time]++;
        }
        PrintWriter pw = new PrintWriter("OUTPUT.TXT");
        int hour, min, sec;
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i]; j++) {
                hour = i / 3600;
                min = (i % 3600)/60;
                sec = i % 60;
                pw.write(hour + " " + min + " " + sec + "\n");
            }
        }
        pw.close();
    }
}
