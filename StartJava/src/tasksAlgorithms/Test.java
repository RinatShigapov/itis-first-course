package tasksAlgorithms;

public class Test {
        static int[] y = {0, 0, 0, 0, 0, 0, 0, 0};
        static int count = 0;

        public static void main(String[] args) {
            putQueen(0);
            System.out.println(count);

        }

        public static void putQueen(int x) {
            if (x == 8) {
                count++;
            } else {
                for (y[x] = 0; y[x] < 8; y[x]++) {
                    if (correct(x)) putQueen(x + 1);
                }
            }
        }

        private static boolean correct(int x) {

            for (int i = 0; i < x; i++) {
                if (y[i] == y[x] || Math.abs(x - i) == Math.abs(y[x] - y[i]))
                    return false;
            }
            return true;
        }
}
