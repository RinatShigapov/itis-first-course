package tasksAlgorithms;

import java.math.BigInteger;
import java.util.Scanner;

public class Task5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        long p = 1000000009;
        int n = scanner.nextInt();
        int m = scanner.nextInt();

        BigInteger a = BigInteger.valueOf(1);

        for (int i = 0; i < n; i++) {
            a = a.multiply(BigInteger.valueOf(m - i)); // m!
        }

        BigInteger b = BigInteger.valueOf(1);
        for (int i = 1; i <= n; i++) {
            b = b.multiply(BigInteger.valueOf(i)); // n!
        }

        BigInteger res = a.divide(b).mod(BigInteger.valueOf(p));

        System.out.println(res);
    }
}
