package tasksAlgorithms;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[] arr = new int[n];
        int s = sc.nextInt();
        int cars = 0;
        for (int i = 0; i < n; i++) {
            arr[i] = sc.nextInt();
        }
        qsort(arr, 0, arr.length - 1);
        for (int i = 0; i < n; i++) {
            if (s < arr[i]){
                break;
            }
            s = s - arr[i];
            cars++;
        }
        PrintWriter fw = new PrintWriter("OUTPUT.TXT");
        fw.print(cars);
        fw.close();
        sc.close();

    }

    public static void qsort(int[] arr, int start, int end) {
        int l = start, r = end;
        int pivot = arr[(start + end) / 2];
        while (l <= r) {
            while (arr[l] < pivot) {
                l++;
            }
            while (arr[r] > pivot) {
                r--;
            }
            if (l <= r) {
                int b = arr[l];
                arr[l] = arr[r];
                arr[r] = b;
                l++;
                r--;
            }
        }
        if (start < r) qsort(arr, start, r);
        if (l < end) qsort(arr, l, end);
    }
}
