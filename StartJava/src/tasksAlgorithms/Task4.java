package tasksAlgorithms;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner sc = new Scanner(System.in);
        String str = "";
        int count = 0;
        int room = Integer.parseInt(sc.nextLine());
        for (int i = 0; i < room; i++) {
            int members = Integer.parseInt(sc.nextLine());
            count += members;
            for (int j = 0; j < members; j++) {
                str += sc.nextLine() + ", ";
            }
        }
        String[] arr = str.split(", ");

        for (int i = 0; i < arr.length - 1; i++) {
            int min = i;
            for (int j = i + 1; j < arr.length; j++) {
                String first = arr[min].substring(0, arr[min].indexOf(" "));
                String second = arr[j].substring(0, arr[j].indexOf(" "));
                if(Double.parseDouble(first) < Double.parseDouble(second)){
                    min = j;
                }
            }
            if (min != i) {
                String str1 = arr[i];
                arr[i] = arr[min];
                arr[min] = str1;
            }
        }

        PrintWriter fw = new PrintWriter("OUTPUT.TXT");
        fw.write(arr.length + "\n");
        for (String s:arr) {
            fw.write(s + "\n");
        }
        fw.close();
        sc.close();
    }
}
