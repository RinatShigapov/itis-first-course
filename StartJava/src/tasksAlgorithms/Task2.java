package tasksAlgorithms;

import java.io.*;

public class Task2 {

    public static void main(String[] args) throws IOException, FileNotFoundException {
        int[] arr = new int[201];
        BufferedReader sc = new BufferedReader(new FileReader(new File("INPUT.TXT")));
        int n = Integer.valueOf(sc.readLine());
        int x;
        int number = 0;
        boolean minus = false;
        for (int i = 0; i < n; i++) {
            x = sc.read();
            do {
                if (x == '-') {
                    minus = true;
                } else {
                    number = number * 10 + (x - '0');
                }
                x = sc.read();
            } while (x != ' ' && x != -1 && x != '\n' && x != '\r');
            if (minus) {
                number = -number;
            }
            arr[100 + number]++;
            minus = false;
            number = 0;
        }
        sc.close();
        PrintWriter out = new PrintWriter("OUTPUT.TXT");
        for (int i = 0; i < 201; i++) {
            int num = i - 100;
            String strNum = num + " ";
            for (int j = 0; j < arr[i]; j++) {
                out.write(strNum);
            }
        }
        out.close();
    }
}


