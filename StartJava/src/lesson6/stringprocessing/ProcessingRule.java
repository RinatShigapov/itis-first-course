package lesson6.stringprocessing;

//функциональный интерфейс, т к ровно один абстрактный метод
public interface ProcessingRule {
    String process(String s);
}