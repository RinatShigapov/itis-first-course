package lesson2.stackQueue;

public class QueueDemo {
    public static void main(String[] args) {
        IntQueue q = new IntQueue();
        q.enqueue(1);
        q.enqueue(2);
        q.enqueue(3);
        System.out.println(q.size());
        System.out.println(q.isEmpty());
        System.out.println(q.dequeue());
        System.out.println(q.first());
    }
}
