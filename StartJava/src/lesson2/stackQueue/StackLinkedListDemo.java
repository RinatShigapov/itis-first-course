package lesson2.stackQueue;

public class StackLinkedListDemo {
    public static void main(String[] args) {
        StackLinkedList st = new StackLinkedList();
        st.push(5);
        System.out.println(st.pop());
        st.push(3);
        System.out.println(st.pop());
        st.push(1);
        System.out.println(st.pop());
    }
}
