package lesson2.stackQueue;

public class IntQueue {
    private Node first;
    private int n;

    public IntQueue() {
        n = 0;
    }

    public void enqueue(int elem) {
        Node newNode = new Node();
        newNode.value = elem;
        if (first != null) {
            Node current = first;
            while (current.next != null) {
                current = current.next;
            }
            current.next = newNode;
        } else {
            first = newNode;
        }
        n++;
    }

    public int dequeue() {
        if (first != null) {
            int a = first.value;
            first = first.next;
            n--;
            return a;
        } else {
            throw new IllegalStateException("Queue is empty");
        }
    }

    public boolean isEmpty() {
        return first == null;
    }

    public int size() {
        return n;
    }

    public int first() {
        if (first != null) {
            return first.value;
        } else throw new IllegalStateException("Queue is empty");
    }

    public class Node {
        int value;
        Node next;
    }
}