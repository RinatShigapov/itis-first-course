package lesson2.stackQueue;

public class StackLinkedList {
    private Node first;
    private int n;

    public StackLinkedList() {
        n = 0;
    }

    public void push(int a) {
        Node newNode = new Node();
        newNode.value = a;
        if (first != null) {
            Node current = first;
            while (current.next != null) {
                current = current.next;
            }
            current.next = newNode;
        } else {
            first = newNode;
        }
        n++;
    }

    public int pop() {
        int i = 0;
        Node current = first;
        int ret;
        if (n == 1) {
            ret = first.value;
            first = null;
        } else if (n == 0) {
            throw new IllegalStateException("Stack is empty");
        } else {
            while (i < n - 2) {
                current = current.next;
                i++;
            }
            ret = current.next.value;
            current.next = null;
        }
        n--;
        return ret;
    }

    public boolean isEmpty() {
        return first == null;
    }

    class Node {
        int value;
        Node next;
    }

}
