package lesson2.node;

public interface IntIterator {
    boolean hasNext();
    int next();
}
