package lesson14;

import java.util.Random;

public class MainApp {
    public static long sum = 0;
    public static long sum2 = 0;

    public static void main(String[] args) throws InterruptedException {
        Random r = new Random();
        int[] arr = new int[1000000];

        for (int i = 0; i < arr.length; i++) {
            arr[i] = r.nextInt();
        }

        Thread t1 = new CounterThread(0, 200000, arr);
        Thread t2 = new CounterThread(200001, 400000, arr);
        Thread t3 = new CounterThread(400001, 600000, arr);
        Thread t4 = new CounterThread(600001, 800000, arr);
        Thread t5 = new CounterThread(800001, 1000000-1, arr);

        t1.start();
        t2.start();
        t3.start();
        t4.start();
        t5.start();

        t1.join();
        t2.join();
        t3.join();
        t4.join();
        t5.join();

        for (int a:
             arr) {
            sum2 += a;
        }

        System.out.println(sum);
        System.out.println(sum2);
    }
}
