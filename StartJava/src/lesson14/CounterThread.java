package lesson14;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class CounterThread extends Thread {
    private int start;
    private int end;
    private int[] array;

    private static final Object MUTEX = new Object();
    private static Lock lock = new ReentrantLock();

    public CounterThread(int start, int end, int[] array) {
        this.start = start;
        this.end = end;
        this.array = array;
    }

    @Override
    public void run() {
        for (int i = start; i <= end ; i++) {
//            synchronized (MUTEX) {
            lock.lock();
                MainApp.sum += array[i];
                lock.unlock();
//            }
        }
    }
}
