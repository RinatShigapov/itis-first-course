package lesson15;

public class Consumer extends Thread{
    private Product product;

    public Consumer(Product product) {
        this.product = product;
    }

    @Override
    public void run() {
        while (true) {
            synchronized (product) {
                while (!product.isProduce()) {
                    try {
                        System.out.println("Еще не готов");
                        product.wait();
                    } catch (InterruptedException e) {
                        throw new IllegalStateException(e);
                    }
                }
                product.consume();
                System.out.println("Потребил");
                product.notify();
            }
        }
    }
}
