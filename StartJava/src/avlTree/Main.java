package avlTree;

public class Main {
    public static void main(String[] args) {
        AVLTree tree = new AVLTree();

        /* Constructing tree given in the above figure */
        tree.root = tree.insert(9);
        tree.root = tree.insert(5);
        tree.root = tree.insert(10);
        tree.root = tree.insert(0);
        tree.root = tree.insert(6);
        tree.root = tree.insert(11);
        tree.root = tree.insert(-1);
        tree.root = tree.insert(1);
        tree.root = tree.insert(2);

		/* The constructed AVL Tree would be
		 9
		/ \
	   1  10
	  / \  \
	 0  5   11
	 /  / \
   -1  2  6
		*/
        System.out.println("Preorder traversal of " +
                "constructed tree is : ");
        tree.preOrder(tree.root);
        System.out.println();
        tree.printTreeByLevels();
        System.out.println();

        tree.root = tree.deleteNode(10);

		/* The AVL Tree after deletion of 10
		1
	   / \
	  0   9
	 /	 / \
	-1  5  11
	   / \
	  2  6
		*/
        System.out.println("Preorder traversal after " +
                "deletion of 10 :");
        tree.preOrder(tree.root);
        System.out.println();
        tree.printTreeByLevels();
        System.out.println();

        int min = tree.minValue();
        System.out.println("Минимальное значение в дереве: " + min);//поиск и удаление минимального значения
        tree.root = tree.deleteNode(tree.minValue());
        System.out.println(tree.contains(min));// проверка на удаление
    }
}
