package avlTree;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class CheckTimes {
    public static void main(String[] args) throws IOException {
        File f = new File("Number.txt");
        File insert = new File("insertAVL.txt");
        FileWriter fw = new FileWriter(insert);
        File contains = new File("containsAVL.txt");
        FileWriter fwCon = new FileWriter(contains);
        File delete = new File("deleteAVL.txt");
        FileWriter fwDel = new FileWriter(delete);
        Scanner sc = new Scanner(f);

        while (sc.hasNextLine()) {
            String str = sc.nextLine();
            String[] strArrays = str.split(" ");
            int[] arr = new int[strArrays.length - 1];
            for (int i = 0; i < arr.length; i++) {
                arr[i] = Integer.parseInt(strArrays[i + 1]);
            }

            AVLTree tree = new AVLTree();
            long begin, end, result;
            long sumTime = 0;
            long sumTimeDelete = 0;
            long sumTimeContains = 0;
            for (int i = 0; i < arr.length; i++) {
                begin = System.nanoTime();
                tree.root = tree.insert(arr[i]);
                end = System.nanoTime();
                result = end - begin;
                sumTime += result;
            }
            for (int i = 0; i < arr.length; i++) {
                begin = System.nanoTime();
                tree.contains(arr[i]);
                end = System.nanoTime();
                result = end - begin;
                sumTimeContains += result;
            }
            for (int i = 0; i < arr.length; i++) {
                begin = System.nanoTime();
                tree.root = tree.deleteNode(arr[i]);
                end = System.nanoTime();
                result = end - begin;
                sumTimeDelete += result;
            }
            fw.write((sumTime/arr.length) + "\n");
            fwCon.write((sumTimeContains/arr.length) + "\n");
            fwDel.write((sumTimeDelete/arr.length) + "\n");
        }
        fw.close();
        fwCon.close();
        fwDel.close();
    }
}
