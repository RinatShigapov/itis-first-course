package lesson13.v1;

public class NoteUser {
    private Notepad notepad;

    public NoteUser(Notepad notepad) {
        this.notepad = notepad;
    }

    private String userName;

    public Notepad getNotepad() {
        return notepad;
    }

    public void setNotepad(Notepad notepad) {
        this.notepad = notepad;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    void takeNotes(int count) {
        for (int i = 0; i < count; i++) {
            notepad.addNote("Some note");
        }
    }
}
