package lesson4;

public class Collisions {
    public static void main(String[] args) {
        int[] arr = new int[16];
        for (int i = 0; i < 10000; i++) {
            String s = getRandomString();
            int hashcode = Math.abs(s.hashCode());
            int index = hashcode % 16;
            arr[index]++;
        }
        for (int i = 0; i < 16; i++) {
            System.out.println(arr[i]);
        }
    }
    static String getRandomString(){
        String str = "";
        for (int i = 0; i < 10; i++) {
            int number = (int) (Math.random() * 97 + 25);
            char a = (char) number;
            str = str + a;
        }
        return str;
    }
}
