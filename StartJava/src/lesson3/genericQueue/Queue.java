package lesson3.genericQueue;

public class Queue<T> {
    private Node first;
    private Node last;
    private int n;

    public Queue() {
        n = 0;
    }

    public void enqueue(T elem) {
        Node oldLast = last;
        last = new Node();
        last.value = elem;
        last.next = null;
        if (isEmpty()) {
            first = last;
        } else {
            oldLast.next = last;
        }
        n++;
    }

    public T dequeue() {
        if (!isEmpty()) {
            T a = first.value;
            first = first.next;
            n--;
            return a;
        } else {
            throw new IllegalStateException("Queue is empty");
        }
    }

    public boolean isEmpty() {
        return first == null;
    }

    class Node {
        T value;
        Node next;
    }
}
