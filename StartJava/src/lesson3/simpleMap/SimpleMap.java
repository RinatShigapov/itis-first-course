package lesson3.simpleMap;

public class SimpleMap<K, V> implements Map<K, V> {
    private static final int SIZE = 10;
    private final static double COEFFICENT = 1.5;
    Entry<K, V>[] entries;
    private int n;


    public SimpleMap() {
        this.entries = new Entry[SIZE];
        this.n = 0;
    }

    @Override
    public void put(K key, V value) {
        if (n == entries.length) {
            Entry<K, V>[] newArray = new Entry[(int) (entries.length * COEFFICENT)];
            for (int i = 0; i < entries.length; i++) {
                newArray[i] = entries[i];
            }
            entries = newArray;
        }
        for (int i = 0; i < n; i++) {
            if (entries[i].key.equals(key)) {
                entries[i].value = value;
                return;
            }
        }
        entries[n++] = new Entry<>(key, value);
    }

    @Override
    public V get(K key) {
        for (int i = 0; i < n; i++) {
            if (entries[i].key.equals(key)) {
                return entries[i].value;
            }
        }
        return null;
    }

    public K[] getKeys() {
        K[] k = (K[])new Object[n];
        for (int i = 0; i < n; i++) {
            k[i] = entries[i].key;
        }
        return k;
    }

    class Entry<I, O> {
        I key;
        O value;

        public Entry(I key, O value) {
            this.key = key;
            this.value = value;
        }
    }
}
