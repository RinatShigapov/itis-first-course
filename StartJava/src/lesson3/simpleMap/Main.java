package lesson3.simpleMap;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws FileNotFoundException {
        //Map<String, Integer> m1 = new SimpleMap<>();
        //Map m2 = new SimpleMap();//из-за обратной совместимости

        File f = new File("Book");
        Scanner sc = new Scanner(f);
        SimpleMap<String, Integer> map = new SimpleMap<>();
        while (sc.hasNext()) {
            String str = sc.next().replace(",","").replace(".","").replace("-","");
            if (map.get(str) == null) {
                map.put(str, 1);
            } else map.put(str, map.get(str) + 1);
            System.out.println(map.get(str));
        }
        Object[] a = map.getKeys();
        System.out.println(a[0]);
    }
}
