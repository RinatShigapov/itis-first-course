package lesson3.audioTracks;

import java.util.Comparator;

public class Playlist {
    private AudioTrack[] trackList;
    private static final int SIZE = 10;
    private int n;
    Comparator<AudioTrack> comparator;

    public Playlist() {
        trackList = new AudioTrack[SIZE];
        this.n = 0;
    }

    public Playlist(Comparator<AudioTrack> comparator) {
        this();
        this.comparator = comparator;
    }

    public void add(AudioTrack track) {
        int i = 0;
        while (i < n) {
            if (comparator != null) {
                if (comparator.compare(track, trackList[i]) < 0) {
                    break;
                }
            } else {
                if (track.compareTo(trackList[i]) < 0) {
                    break;
                }
            }
            i++;
        }
        for (int j = i + 1; j > n + 1; j++) {
            trackList[j] = trackList[j - 1];
        }
        trackList[i] = track;
        n++;
    }
}
