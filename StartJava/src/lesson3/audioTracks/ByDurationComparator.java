package lesson3.audioTracks;

import java.util.Comparator;

public class ByDurationComparator implements Comparator<AudioTrack> {
    // если о1 больше о2 , то вернет число > 0
    @Override
    public int compare(AudioTrack o1, AudioTrack o2) {
        return o1.getDuration() - o2.getDuration();
    }
}
