package lesson3.audioTracks;

import java.util.Comparator;

public class Main {
    public static void main(String[] args) {
        AudioTrack a1 = new AudioTrack("adc", "rwf", 32);
        AudioTrack a2 = new AudioTrack("twvwv", "rwfvwrgfv", 312);
        AudioTrack a3 = new AudioTrack("adc", "rwf", 32);

        Comparator<AudioTrack> c = new ByDurationComparator();
        Playlist p = new Playlist(c);
        p.add(a1);
        p.add(a2);
        p.add(a3);
        System.out.println(a1.equals(a3));
    }
}
