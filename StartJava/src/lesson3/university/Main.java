package lesson3.university;

import java.util.Comparator;

public class Main {
    public static void main(String[] args) {
        Student s1 = new Student("R", "Sh", 1, 803);
        Student s2 = new Student("B", "A", 1, 803);
        Student s3 = new Student("F", "E", 1, 803);
        System.out.println(s1.equals(s2));

        Comparator<Student> comp = new BySurnameComparator();
        Group g803 = new Group(comp);
        Group g802 = new Group(comp);
        g802.add(s2);
        g803.add(s1);
        g803.add(s2);
        g803.add(s3);
        System.out.println(g803.Surname(2));
        System.out.println(g802.hashCode());
        System.out.println(g803.hashCode());

    }
}
