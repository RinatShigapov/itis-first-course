package lesson3.university;

import java.util.Comparator;

public class ByNumberOfGroupComparator implements Comparator<Student> {
    @Override
    public int compare(Student o1, Student o2) {
        return o1.getNumberOfGroup() - o2.getNumberOfGroup();
    }
}
