package lesson3.university;

public class Student implements Comparable<Student> {
    private String name;
    private String surname;
    private int numberOfGroup;
    private int course;


    public Student(String name, String surname, int course, int numberOfGroup) {
        this.name = name;
        this.surname = surname;
        this.numberOfGroup = numberOfGroup;
        this.course = course;
    }


    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public int getNumberOfGroup() {
        return numberOfGroup;
    }

    public int getCourse() {
        return course;
    }


    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Student)) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        Student a = (Student) obj;
        return this.course == a.getCourse() &&
                this.name == a.getName() &&
                this.surname == a.getSurname() &&
                this.numberOfGroup == a.getNumberOfGroup();
    }

    @Override
    public int compareTo(Student s) {
        return this.surname.compareTo(s.getSurname());
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = 31 * result + name.hashCode();
        result = 31 * result + surname.hashCode();
        result = 31 * result + numberOfGroup;
        result = 31 * result + course;
        return result;
    }

}
