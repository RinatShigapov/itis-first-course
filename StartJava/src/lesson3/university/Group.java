package lesson3.university;

import java.util.Comparator;

public class Group {
    private Student[] group;
    public static final int SIZE = 25;
    private int n;
    Comparator<Student> comparator;

    public Group() {
        group = new Student[SIZE];
        n = 0;
    }

    public Group(Comparator<Student> comparator) {
        this();
        this.comparator = comparator;
    }

    public void add(Student student) {
        int i = 0;
        while (i < n) {
            if (comparator != null) {
                if (comparator.compare(student, group[i]) < 0) {
                    break;
                }
            } else {
                if (student.compareTo(group[i]) < 0) {
                    break;
                }
            }
            i++;
        }
        for (int j = i + 1; j < n + 1; j++) {
            group[j] = group[j - 1];
        }
        group[i] = student;
        n++;
    }

    public String Surname(int index) {
        if (group[index] != null) {
            return group[index].getSurname();
        } else {
            return "This element is empty";
        }
    }
}
