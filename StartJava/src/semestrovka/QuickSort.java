package semestrovka;

import java.util.LinkedList;

public class QuickSort {

    private int iter;

    public void qsort(int[] arr, int start, int end) {
        int l = start, r = end;
        int pivot = arr[(start + end) / 2];
        while (l <= r) {
            while (arr[l] < pivot) {
                l++;
                iter++;
            }
            while (arr[r] > pivot) {
                r--;
                iter++;
            }
            if (l <= r) {
                swap(arr, l, r);
                l++;
                r--;
            }
        }
        if (start < r) qsort(arr, start, r);
        if (l < end) qsort(arr, l, end);
    }

    public static void swap(int[] a, int i, int j) {
        int b = a[i];
        a[i] = a[j];
        a[j] = b;
    }

    public int getIter() {
        return iter;
    }


    public int[] readForLinkedList(LinkedList<Integer> list) {
        int[] arr = new int[list.size()];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = list.remove();
        }
        return arr;
    }

    public LinkedList<Integer> qsortForLinkedList(LinkedList<Integer> list) {
        int[] arr = new int[list.size()];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = list.remove();
        }
        qsort(arr, 0, arr.length - 1);
        for (int i = 0; i < arr.length; i++) {
            list.add(arr[i]);
        }
        return list;
    }

}
