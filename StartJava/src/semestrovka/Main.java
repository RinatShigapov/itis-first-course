package semestrovka;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws IOException {
        File f = new File("Number.txt");
        File res = new File("Result.txt");
        FileWriter fw = new FileWriter(res);
        Scanner sc = new Scanner(f);
        int sumIter = 0;
        int sumTime = 0;

        while (sc.hasNextLine()) {
            String str = sc.nextLine();
            String[] strArrays = str.split(" ");
            int[] arr = new int[strArrays.length - 1];
            for (int i = 0; i < arr.length; i++) {
                arr[i] = Integer.parseInt(strArrays[i + 1]);
            }
            QuickSort q = new QuickSort();
            long begin, end, result;
            begin = System.nanoTime();
            q.qsort(arr, 0, arr.length - 1);
            end = System.nanoTime();
            result = end - begin;
            fw.write(Arrays.toString(arr) + "\n");
//            System.out.println("\n" + result);
//            System.out.println(q.getIter());
            sumIter += q.getIter();
            sumTime += result;
        }
        System.out.print("\n" + sumIter/100);
        System.out.println(" " + sumTime/100);
        sc.close();
        fw.close();

//        while (sc.hasNextLine()) {
//            LinkedList<Integer> list = doList(sc.nextLine());
//            QuickSort q = new QuickSort();
//            long begin, end, result;
//            begin = System.nanoTime();
//            int[] arr = q.readForLinkedList(list);
//            q.qsort(arr, 0, arr.length - 1);
//            for (int i = 0; i < arr.length; i++) {
//                list.add(arr[i]);
//            }
//            end = System.nanoTime();
//            result = end - begin;
//            for (int i = 0; i < list.size(); i++) {
//                fw.write(list.get(i) + " ");
//            }
//            fw.write("\n");
//            System.out.println("\n" + result);
//            sumIter += q.getIter();
//            sumTime += result;
//        }
//        System.out.println("\n" + sumIter / 100);
//        System.out.println("\n" + sumTime / 100);
//        sc.close();
//        fw.close();

    }

//    public static LinkedList<Integer> doList(String str) {
//        String[] strArrays = changeStringtoArray(str);
//        LinkedList<Integer> list = new LinkedList<>();
//        for (int i = 1; i < strArrays.length - 1; i++) {
//            list.add(Integer.parseInt(strArrays[i]));
//        }
//        return list;
//    }
//
//    public static String[] changeStringtoArray(String str) {
//        String[] strArrays = str.split(" ");
//        return strArrays;
//    }




}
