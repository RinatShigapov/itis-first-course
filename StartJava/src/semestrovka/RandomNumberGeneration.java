package semestrovka;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;

public class RandomNumberGeneration {
    private static final int SIZE = 100;
    static Random r = new Random();

    public static void main(String[] args) throws IOException {
        File f = new File("Number.txt");
        cleanFile();
        FileWriter fileWriter = new FileWriter(f, true);
        int sizeString, number, length;
        String numbers;
        length = 100;
        fileWriter.write("");
        for (int i = 0; i < SIZE; i++) {
            sizeString = length;
            numbers = "";
            for (int j = 0; j < sizeString; j++) {
                number = r.nextInt(10000);
                numbers = numbers + " " + number;
            }
            fileWriter.write(numbers + "\n");
            length += 100;
        }
        fileWriter.close();
    }

    public static void cleanFile() throws IOException {
        FileWriter fileWriter = new FileWriter("Number.txt", false);
        fileWriter.write("");
        fileWriter.close();
    }
}
