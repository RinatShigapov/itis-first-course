package lesson11;

public class Main {
    public static void main(String[] args) {
        BinarySearchTree<Integer> bst = new BinarySearchTreeImpl<>();
        bst.insert(10);
        bst.insert(20);
        bst.insert(15);
        bst.insert(18);
        bst.insert(30);
        bst.insert(40);
        bst.insert(5);
        bst.insert(4);
        bst.insert(6);
        bst.printAll();
        System.out.println(bst.remove(10));
        bst.printAllByLevels();
    }
}
