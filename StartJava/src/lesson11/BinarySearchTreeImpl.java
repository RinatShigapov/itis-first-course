package lesson11;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;

public class BinarySearchTreeImpl<T extends Comparable<T>>
        implements BinarySearchTree<T> {

    private class TreeNode {
        T value;
        TreeNode left;
        TreeNode right;

        public TreeNode(T value) {
            this.value = value;
        }
    }

    private TreeNode root;

    @Override
    public void insert(T elem) {
        this.root = insert(this.root, elem);
    }

    private TreeNode insert(TreeNode root, T elem) {
        if (root == null) {
            root = new TreeNode(elem);
        } else {
            if (root.value.compareTo(elem) >= 0) {
                root.left = insert(root.left, elem);
            } else {
                root.right = insert(root.right, elem);
            }
        }
        return root;
    }

    public boolean remove(T elem) {
        TreeNode parent = null;
        TreeNode current = root;

        while (current != null) {
            if (current.value.compareTo(elem) > 0) {
                parent = current;
                current = current.left;
            } else if (current.value.compareTo(elem) < 0) {
                parent = current;
                current = current.right;
            } else {
                break;
            }
        }

        if (current == null) {
            return false;
        }

        if (current.right == null && current.left == null) {
            if (parent == null) {
                root = null;
            } else if (parent.left == current) {
                parent.left = null;
            } else if (parent.right == current) {
                parent.right = null;
            }
        } else if (current.right == null) {
            if (parent == null) {
                root = root.left;
            } else if (parent.right == current) {
                parent.right = current.left;
            } else if (parent.left == current) {
                parent.left = current.left;
            }
        } else if (current.left == null) {
            if (parent == null) {
                root = root.right;
            } else if (parent.right == current) {
                parent.right = current.right;
            } else if (parent.left == current) {
                parent.left = current.right;
            }
        } else {
            TreeNode helper;
            TreeNode parentHelper = null;

            helper = current.left;

            while (helper.right != null) {
                parentHelper = helper;
                helper = helper.right;
            }

            if (current.left != helper) {
                helper.left = current.left;
            }


            if (parent == null) {
                if (helper != root.left) {
                    helper.left = root.left;
                    helper.right = root.right;
                } else {
                    helper.right = root.right;
                }
                root = helper;
                if (parentHelper != null)
                    parentHelper.right = null;


            } else {
                if (parent.right == current) {
                    helper.right = current.right;
                    parent.right = helper;

                } else parent.left = helper;

            }
        }

        return true;
    }

    @Override
    public boolean contains(T elem) {
        return contains(elem, root);
    }

    private boolean contains(T elem, TreeNode node) {
        if (node == null) {
            return false;
        } else if (node.value.compareTo(elem) == 0) {
            return true;
        } else {
            if (node.value.compareTo(elem) >= 0) {
                return contains(elem, node.left);
            } else {
                return contains(elem, node.right);
            }
        }
    }

    @Override
    public void printAll() {
        printAll(this.root);
    }

    private void printAll(TreeNode root) {
        if (root != null) {
            printAll(root.left);
            System.out.println(root.value);
            printAll(root.right);
        }
    }

    @Override
    public void printAllByLevels() {
        Queue<TreeNode> currentLevel = new LinkedList<TreeNode>();
        Queue<TreeNode> nextLevel = new LinkedList<TreeNode>();

        currentLevel.add(root);

        while (!currentLevel.isEmpty()) {
            Iterator<TreeNode> iter = currentLevel.iterator();
            while (iter.hasNext()) {
                TreeNode currentNode = iter.next();
                if (currentNode.left != null) {
                    nextLevel.add(currentNode.left);
                }
                if (currentNode.right != null) {
                    nextLevel.add(currentNode.right);
                }
                System.out.print(currentNode.value + " ");
            }
            System.out.println();
            currentLevel = nextLevel;
            nextLevel = new LinkedList<TreeNode>();

        }
    }
}

