package PATTERNS;

public class Singleton {
    private static Singleton president = null;
    public static Singleton getPresident(){
        if (president == null){
            president = new Singleton();
        }
        return president;
    }
}
