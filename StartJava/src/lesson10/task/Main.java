package lesson10.task;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {

        List<String> l = new ArrayList<>();
        l.add("asdfa3e");
        l.add("ertr");
        l.add("a3e");
        l.add("qpoerituertAIHKJFH");

        List<Integer> lengths = l.stream()
                .map(s -> s.length())
                .collect(Collectors.toList());
        System.out.println(lengths);

        MyArrayList<String> list = new MyArrayList<>();
        list.add("asdfa3e");
        list.add("ertr");
        list.add("a3e");
        list.add("qpoerituertAIHKJFH");
        List<Integer> lengths2 = list.stream()
                .map(s -> s.length())
                .collect(Collectors.toList());
        System.out.println(lengths2);
    }
}
