package lesson10.lamdasrep;

public interface MyFunction {

    String process(String s);
}
