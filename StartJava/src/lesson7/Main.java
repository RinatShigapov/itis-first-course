package lesson7;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

public class Main {
    public static void main(String[] args) throws IOException {
        InputStream is = new FileInputStream("input.txt");
        System.out.println(is.read());
        System.out.println(is.read());
        System.out.println(is.read());
    }
}
