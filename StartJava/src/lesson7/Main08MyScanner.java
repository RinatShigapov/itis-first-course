package lesson7;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

public class Main08MyScanner {
    public static void main(String[] args) throws IOException {
        InputStream is = new FileInputStream("input.txt");
        MyScanner m = new MyScanner(is);
//        System.out.println(m.nextInt());
//        System.out.println(m.nextDouble());
//        System.out.println(m.next());
//        System.out.println(m.next());
        System.out.println(m.nextLine());
    }
}
