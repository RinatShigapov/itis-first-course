package lesson7;

import java.io.IOException;
import java.io.InputStream;
import java.util.InputMismatchException;

public class MyScanner {
    private InputStream is;

    public MyScanner(InputStream is) {
        this.is = is;
    }


    public int nextInt() throws IOException {
        int i;
        String str = "";
        while (((i = is.read()) != ' ') && (i != -1) && (i != 10)) {
            if ((i > 47) && (i < 58)) {
                str = str + (char) i;
            } else throw new InputMismatchException();
        }
        int res = Integer.parseInt(str);
        return res;
    }

    public double nextDouble() throws IOException {

        int i;
        String str = "";
        while (((i = is.read()) != ' ') && (i != -1) && (i != 10)) {
            if (((i > 47) && (i < 58)) || (i == '.')) {
                str = str + (char) i;
            } else throw new InputMismatchException();
        }
        double res = Double.parseDouble(str);
        return res;
    }

    public String next() throws IOException {
        int i;
        String str = "";
        while (((i = is.read()) != ' ') && (i != -1) && (i != 10)) {
            str = str + (char) i;
        }
        return str;
    }

    public String nextLine() throws IOException {
        int i;
        String str = "";
        while (((i = is.read()) != -1) && (i != 10)) {
            str = str + (char) i;
        }
        return str;
    }
}
