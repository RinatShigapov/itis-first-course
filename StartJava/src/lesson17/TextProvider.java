package lesson17;

public interface TextProvider {
    String getText();
}