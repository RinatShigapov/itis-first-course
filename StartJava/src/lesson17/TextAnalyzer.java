package lesson17;

public interface TextAnalyzer {
    double analyze(TextProvider tp1,
                   TextProvider tp2);
}