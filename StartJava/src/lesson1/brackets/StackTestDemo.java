package lesson1.brackets;

public class StackTestDemo {
    public static void main(String[] args) {
        Stack s = new Stack(10);
        s.push('h');
        s.push('e');
        s.push('l');
        s.push('l');
        s.push('o');
        System.out.print(s.pop());
        System.out.print(s.pop());
        System.out.print(s.pop());
        System.out.print(s.pop());
        System.out.print(s.pop());


    }
}
