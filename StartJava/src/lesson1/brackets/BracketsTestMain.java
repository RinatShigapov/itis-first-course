package lesson1.brackets;

import java.util.Scanner;

public class BracketsTestMain {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String s = sc.nextLine();
        boolean b = areBracketsCorrect(s);
        System.out.println(b);
    }

    static boolean areBracketsCorrect(String str) {
        Stack stack = new Stack(100);
        char[] symbols = str.toCharArray();
        for (char c : symbols) {
            try {
                if (c == '(' || c == '[' || c == '{') {
                    stack.push(c);
                } else if (c == ')') {
                    if ((int) (stack.pop()) != (c - 1)) {
                        return false;
                    }
                } else if (c == '}' || c == ']') {
                    if ((int) (stack.pop()) != (c - 2)) {
                        return false;
                    }
                }
            } catch (IllegalStateException e) {
                return false;
            }
        }
        if (!stack.isEmpty()) {
            return false;
        }

        return true;
    }
}
