package lesson1.lists;

import lesson2.node.IntIterator;

public class ArrayIntListDemo {
    public static void main(String[] args) {
        ArrayIntList list = new ArrayIntList();
        IntList list2 = new ArrayIntList();
        list2.add(10);
        list2.add(11);
        list2.add(11);
        IntIterator iter = list2.iterator();
        int sum = 0;
        while (iter.hasNext()) {
            sum += iter.next();
        }
        iter = list.iterator();
        System.out.println(sum);
        /*list.add(5);
        list.add(9);
        list.add(9);
        list.add(5);
        list.add(6);
        list.remove(4);
        list.add(2,1);
        list2.addAll(list,1);
        System.out.println(list.size());
        int[] a = list.toArray();
        for (int i = 0; i < a.length; i++) {
            System.out.println(a[i]);
        }
        System.out.println(list.lastIndexOf(6));
        list.sort();
        for (int i = 0; i < list2.size() ; i++) {
           System.out.println(list2.get(i));
        }
        */
    }
}
