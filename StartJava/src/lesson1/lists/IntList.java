package lesson1.lists;

import lesson2.node.IntIterator;

public interface IntList {
    /**
     * Adds number to the end of the list
     * @param elem - number to add
     */
    void add(int elem);

    void add(int elem, int position);

    /**
     * Returns the element with specified index
     */
    int get(int index);

    int remove(int index);

    int size();

    //возвращает содержимое списка в виде массива
    int[] toArray();

    //упорядочивает числа в списке по возрастанию
    void sort();

    //вставляет в данный список все элементы из List, начиная с position в данном списке
    void addAll(IntList list, int position);

    int lastIndexOf(int elem);

    IntIterator iterator();
}
