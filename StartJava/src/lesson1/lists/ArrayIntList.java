package lesson1.lists;

import lesson2.node.IntIterator;

public class ArrayIntList implements IntList {
    private final static int INITIAL_CAPACITY = 10;
    private final static double COEFFICENT = 1.5;
    private int[] arr;
    private int n;

    public ArrayIntList() {
        arr = new int[INITIAL_CAPACITY];
        n = 0;
    }

    @Override
    public void add(int elem) {
        if (n == arr.length) {
            int[] newArray = new int[(int) (arr.length * COEFFICENT)];
            for (int i = 0; i < arr.length; i++) {
                newArray[i] = arr[i];
            }
            arr = newArray;
        }
        arr[n++] = elem;
    }

    @Override
    public void add(int elem, int position) {
        if (position < n && position >= 0) {
            if (n == arr.length) {
                int[] newArray = new int[(int) (arr.length * COEFFICENT)];
                for (int i = 0; i < arr.length; i++) {
                    newArray[i] = arr[i];
                }
                arr = newArray;
            }
            for (int i = n; i > position; i--) {
                arr[i] = arr[i - 1];
            }
            arr[position] = elem;
        } else throw new IndexOutOfBoundsException("No such element with index = " + position);
    }

    @Override
    public int get(int index) {
        return arr[index];
    }

    @Override
    public int remove(int index) {
        int a = arr[index];
        for (int i = index + 1; i < n; i++) {
            arr[i - 1] = arr[i];
        }
        n--;
        return a;
    }

    @Override
    public int size() {
        return n;
    }

    @Override
    public int[] toArray() {
        int[] ret = new int[n];
        for (int i = 0; i < n; i++) {
            ret[i] = arr[i];
        }
        return ret;
    }

    @Override
    public void sort() {
        for (int i = n - 1; i >= 0; i--) {
            for (int j = 0; j < i; j++) {
                if (arr[j] > arr[j + 1]) {
                    int a = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = a;
                }
            }
        }
    }


    @Override
    public void addAll(IntList list, int position) {
        if (position < n && position >= 0) {
            if (arr.length - n < list.size()) {
                int[] newArray = new int[arr.length + list.size()];
                for (int i = 0; i < arr.length; i++) {
                    newArray[i] = arr[i];
                }
                arr = newArray;
            }
            for (int i = n - 1; i >= position; i--) {
                arr[i + list.size()] = arr[i];
            }
            for (int i = 0; i < list.size(); i++) {
                arr[position + i] = list.get(i);
            }
            n += list.size();
        } else throw new IndexOutOfBoundsException("No such element with index = " + position);
    }

    @Override
    public int lastIndexOf(int elem) {
        for (int i = n - 1; i >= 0; i--) {
            if (arr[i] == elem) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public IntIterator iterator() {
        return new IntIteratorImpl();
    }

    class IntIteratorImpl implements IntIterator {
        int a;

        public IntIteratorImpl() {
            a = 0;
        }

        @Override
        public boolean hasNext() {
            return a != n;
        }

        @Override
        public int next() {
            int valueToRerurn = arr[a];
            a++;
            return valueToRerurn;
        }
    }
}
