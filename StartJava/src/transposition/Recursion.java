package transposition;

public class Recursion {
    public static void transpositionRecursion(String str, int f) {
        if (f == 0){
            System.out.println(str);
        }
        if (f == str.length()){
            return;
        }
        char[] arr = str.toCharArray();
        int l = f + 1;
        for (int i = l; i < arr.length; i++) {
            swap(arr, f, i);
            String st1 = "";
            for (char c : arr) {
                System.out.print(c);
                st1 += c;
            }
            System.out.println();
            transpositionRecursion(st1, f + 1);
            swap(arr, i, f);
        }
        transpositionRecursion(str, f + 1);
    }

    public static void swap(char[] a, int i, int j) {
        char b = a[i];
        a[i] = a[j];
        a[j] = b;
    }

    public static void main(String[] args) {
        transpositionRecursion("abc", 0);
    }
}
