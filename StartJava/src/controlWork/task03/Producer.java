package controlWork.task03;

import java.io.*;

public class Producer extends Thread {
    private Product product;
    private InputStream is;

    public Producer(Product product, String filename) throws FileNotFoundException {
        this.product = product;
        this.is = new FileInputStream(filename);
    }

    @Override
    public void run() {
        while (true) {
            synchronized (product) {
                while (!product.isConsumed()) {
                    try {
                        product.wait();
                    } catch (InterruptedException e) {
                        throw new IllegalStateException(e);
                    }
                }
                try {
                    int a = is.read();
                    product.setElem(a);
                    product.produce();
                    System.out.println("read");
                    product.notify();
                    if (a == -1) {
                        return;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
