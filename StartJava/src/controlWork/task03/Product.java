package controlWork.task03;

public class Product {
    private boolean isReady;
    private int elem;

    public int getElem() {
        return elem;
    }

    public void setElem(int elem) {
        this.elem = elem;
    }

    public boolean isProduced(){
        return isReady;
    }

    public boolean isConsumed(){
        return !isReady;
    }

    public void produce(){
        isReady = true;
    }

    public void consume(){
        isReady = false;
    }
}
