package controlWork.task03;

import java.io.FileNotFoundException;

public class Main {
    public static void main(String[] args) throws FileNotFoundException {
        Product product = new Product();
        Producer producer = new Producer(product, "text1.txt");
        Consumer consumer = new Consumer(product, "text2.txt");

        producer.start();
        consumer.start();
    }
}
