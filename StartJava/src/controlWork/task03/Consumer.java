package controlWork.task03;

import java.io.*;

public class Consumer extends Thread {
    private Product product;
    private OutputStream os;

    public Consumer(Product product, String filename) throws FileNotFoundException {
        this.product = product;
        this.os = new FileOutputStream(filename);
    }

    @Override
    public void run() {
        while (true) {
            synchronized (product) {
                while (!product.isProduced()) {
                    try {
                        product.wait();
                    } catch (InterruptedException e) {
                        throw new IllegalStateException(e);
                    }
                }
                if (product.getElem() == -1) {
                    break;
                }
                try {
                    os.write((char) product.getElem());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                product.consume();
                System.out.println("wrote");
                product.notify();
            }
        }
    }
}
