package controlWork.task02;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Team {
    private List<HockeyPlayer> list;

    public Team() {
        this.list = new ArrayList<>();
    }

    public List<HockeyPlayer> topN(Comparator<HockeyPlayer> comparator, int n){
        List<HockeyPlayer> res = new ArrayList<>();
        list.sort(comparator);
        for (int i = 0; i < n; i++) {
            res.add(list.get(i));
        }
        return res;
    }

    public void add(HockeyPlayer hockeyplayer){
        list.add(hockeyplayer);
    }
}
