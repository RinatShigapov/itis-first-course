package controlWork.task02;

import java.util.Comparator;

public class BySpeedComparator implements Comparator<HockeyPlayer> {
    @Override
    public int compare(HockeyPlayer o1, HockeyPlayer o2) {
        return o2.getSpeed() - o1.getSpeed();
    }
}
