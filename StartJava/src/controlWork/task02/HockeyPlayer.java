package controlWork.task02;

public class HockeyPlayer {
    private int speed;
    private int smart;
    private int power;
    private Team team;

    public HockeyPlayer(int speed, int smart, int power, Team team) {
        this.speed = speed;
        this.smart = smart;
        this.power = power;
        this.team = team;
        team.add(this);
    }

    public int getSpeed() {
        return speed;
    }

    public int getSmart() {
        return smart;
    }

    public int getPower() {
        return power;
    }

    public Team getTeam() {
        return team;
    }

    @Override
    public String toString() {
        return "HockeyPlayer{" +
                "speed=" + speed +
                ", smart=" + smart +
                ", power=" + power +
                '}';
    }
}
