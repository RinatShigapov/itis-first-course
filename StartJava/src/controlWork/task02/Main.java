package controlWork.task02;

public class Main {
    public static void main(String[] args) {
        Team team = new Team();
        BySpeedComparator c = new BySpeedComparator();
        HockeyPlayer player1 = new HockeyPlayer(4, 5, 3, team);
        HockeyPlayer player2 = new HockeyPlayer(5, 5, 3, team);
        HockeyPlayer player3 = new HockeyPlayer(2, 5, 3, team);
        HockeyPlayer player4 = new HockeyPlayer(5, 5, 3, team);
        HockeyPlayer player5 = new HockeyPlayer(7, 5, 3, team);
        HockeyPlayer player6 = new HockeyPlayer(6, 5, 3, team);
        System.out.println(team.topN(c, 5));
    }
}
