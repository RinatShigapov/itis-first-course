package controlWork.task01;

import java.io.FileNotFoundException;

import static controlWork.task01.Task01.filter;

public class Main {
    public static void main(String[] args) throws FileNotFoundException {
        System.out.println(filter((String s) -> s.length() > 3));
    }
}
