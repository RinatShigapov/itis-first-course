package controlWork.task01;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;
import java.util.function.Predicate;

public class Task01 {
    public static List<String> filter(Predicate<String> p) throws FileNotFoundException {
        File f = new File("words.txt");
        Scanner sc = new Scanner(f);
        Map<String, Integer> map = new HashMap<>();
        String str;
        while (sc.hasNext()) {
            str = sc.next();
            if (p.test(str)) {
                int a = map.getOrDefault(str, 0);
                map.put(str, a + 1);
            }
        }
        List<String> list = new LinkedList<>(map.keySet());
        list.sort((o1, o2) -> map.get(o2) - map.get(o1));
        return list;
    }
}
