package lesson16;

import lesson2.node.IntLinkedList;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class LinkedListTest {
    private IntLinkedList list;

    @Before
    public void setUp(){
        list = new IntLinkedList();
    }

    @Test
    public void testIsEmpty() {
        boolean expected = true;
        boolean actual = list.isEmpty();
        assertEquals(expected,actual);
    }

    @Test
    public void testGetOn1(){
        list.add(3);
        int expected = 3;
        int actual = list.get(0);
        assertEquals(expected,actual);
    }

    @Test
    public void testGetOn5(){
        list.add(3);
        list.add(5);
        list.add(7);
        list.add(9);
        list.add(11);
        int expected = 11;
        int actual = list.get(4);
        assertEquals(expected,actual);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testGetNegative(){
        list.get(10);
    }

    @Test
    public void testAdd(){
        list.add(3);
        boolean expected = false;
        boolean actual = list.isEmpty();
        assertEquals(expected,actual);
    }

    @Test
    public void testRemove(){
        list.add(3);
        list.remove(0);
        boolean expected = true;
        boolean actual = list.isEmpty();
        assertEquals(expected,actual);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testRemoveNegative(){
        list.remove(5);
    }

}
