package lesson9.builder;

public class Main {
    public static void main(String[] args) {
//        User.Builder b = new User.Builder();
//        b.id(71L).firstname("Rinat").secondname("Shigapov");
        User u = User.builder()
                .id(67L)
                .firstname("Rinat")
                .secondname("Shigapov")
                .build();
    }
}
