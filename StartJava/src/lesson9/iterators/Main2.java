package lesson9.iterators;

public class Main2 {
    public static void main(String[] args) {
        MyListArray mia = new MyListArray();
        mia.add(5);
        mia.add(10);
        mia.add(15);
        for (Integer i:
                mia) {
            System.out.println(i);
        }
    }
}
