package lesson9.iterators;

import java.util.Iterator;

public class Main1 {
    public static void main(String[] args) {
        MyListArray mia = new MyListArray();
        mia.add(5);
        mia.add(10);
        mia.add(15);
        Iterator<Integer> iterator = mia.iterator();
        while (iterator.hasNext()){
            System.out.println(iterator.next());
        }
    }
}
