package lesson12;

public class Student {
    private int age;
    public String name;

    private void passExam(String exam) {
        System.out.println(exam + " сдал на изи");
        System.out.println("я - " + name);
    }

    private void passExam(String exam, int countOfTries) {
        System.out.println(exam + " сдал с " + countOfTries + " попытки");
        System.out.println("Кстати, я " + name);
    }

    private void passExam(int countofDopkas) {
        System.out.println("Я закрыл целых " + countofDopkas + " допок");
    }

    private void passExam(String exam, Integer point) {
        System.out.println("Сдал " + exam + " на " +
                point + " баллов");
    }

    public Student(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Student{" +
                "age=" + age +
                ", name='" + name + '\'' +
                '}';
    }
}
