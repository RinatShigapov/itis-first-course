package lesson12.superBestAwesomeFramework;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

public class SuperBestAwesomeFramework {

    public static <T> List<T> getMany(Class<T> c, int count) {
        List<T> list = new ArrayList<>();
        try {
            for (int i = 0; i < count; i++) {
                list.add(c.newInstance());
            }
        } catch (InstantiationException |
                IllegalAccessException e) {
            throw new IllegalStateException(e);
        }
        return list;
    }

    public static <T> List<T> getMany(Class<T> c, int count,
                                      Object... args) throws NoSuchMethodException, InvocationTargetException {
        List<T> list = new ArrayList<>();
        Class[] classes = new Class[args.length];
        for (int i = 0; i < classes.length; i++) {
            classes[i] = args[i].getClass();
        }

        Constructor<T> constructor = c.getConstructor(classes);
        try{
            for (int i = 0; i < count; i++) {
                list.add(constructor.newInstance(args));
            }
        }catch (InstantiationException | IllegalAccessException e){
            throw new IllegalStateException(e);
        }
        return list;
    }
}
