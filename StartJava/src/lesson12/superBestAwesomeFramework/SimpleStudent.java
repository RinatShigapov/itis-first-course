package lesson12.superBestAwesomeFramework;

public class SimpleStudent {
    private Integer age;
    public String name;

    public SimpleStudent() {
    }

    public SimpleStudent(Integer age, String name) {
        this.age = age;
        this.name = name;
    }

    @Override
    public String toString() {
        return "SimpleStudent{" +
                "age=" + age +
                ", name='" + name + '\'' +
                '}';
    }
}
