package lesson12.superBestAwesomeFramework;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

import static lesson12.superBestAwesomeFramework.SuperBestAwesomeFramework.getMany;

public class Main {
    public static void main(String[] args) throws NoSuchMethodException, InvocationTargetException {
        List<SimpleStudent> list = getMany(SimpleStudent.class, 10);
        List<SimpleStudent> list2 = getMany(SimpleStudent.class, 10, 32, "Agent SMITH");
        System.out.println(list);
        System.out.println(list2);
    }
}
