package SOLID;

public class SingleResponsibility {
    public class Bank{
        public void storage(){} // Хранение счетов
        public void operations(){} //Операции над счетами
    } // нарушается принцип ответственности

    public class Storage{}
    public class Operations{}
    // Решение: создать два разных класса

}
