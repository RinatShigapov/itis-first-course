package SOLID;

public class OpenClosed {
    public class Bank{}

    public class RussianClient{}
    /*Банк работал только в России. Банк развивается, вышел на международную арену.
     Проблема: журнал не рассчитан на иностранных клентов. Валюта(Дублтрование кода).
     */

    public abstract class Clients{}

    public class RussianClients extends Clients{}
    public class ForeignerClients extends Clients{}



}
