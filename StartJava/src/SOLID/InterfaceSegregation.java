package SOLID;

public class InterfaceSegregation {
    //компания создает телефоны
    public interface SmartPhone{
        public void wirelessChanger();
        public void waterResistant();// Все характестики
    }//Проблема: не во всех телефонах, производимые компанией, присутсвуют данные характеристики

    //Решение: разделить интерфейсы
    public interface wirelessChanger{}
    public interface waterResistant{}
}
