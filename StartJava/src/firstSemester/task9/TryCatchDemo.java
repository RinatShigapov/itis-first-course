package firstSemester.task9;

public class TryCatchDemo {
    public static void main(String[] args) {
        try {
            Rectangle r = new Rectangle(-5, 4);
        } catch (MyException2 myException2) {
            myException2.printStackTrace();
            myException2.getMessage();
        }
    }
}
