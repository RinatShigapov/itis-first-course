package firstSemester.task9;

public class Rectangle {
    private int width;
    private int heigth;

    public Rectangle(int width, int heigth) throws MyException2 {
        if (heigth <= 0) {
            throw new MyException("Length must be >= 0");
        }
        if (width <= 0) {
            throw new MyException2("Width must be >= 0");
        }
        this.width = width;
        this.heigth = heigth;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) throws MyException2 {
        if (width <= 0) {
            throw new MyException2("Width must be >= 0");
        }
        this.width = width;
    }

    public int getHeigth() {
        return heigth;
    }

    public void setHeigth(int heigth) {
        if (heigth <= 0) {
            throw new MyException("Length must be >= 0");
        }
        this.heigth = heigth;
    }

    public int getPerimetr() {
        return (heigth + width) * 2;
    }

    public int getArea() {
        return width * heigth;
    }

}
