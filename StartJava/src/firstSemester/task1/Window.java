package firstSemester.task1;

public class Window {
    private int widht;
    private int height;
    private boolean isLocked = true;

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWidht() {
        return widht;
    }

    public void setWidht(int widht) {
        this.widht = widht;
    }

    public boolean isLocked() {
        return isLocked;
    }

    public void setLocked(boolean locked) {
        if(locked && isLocked){
            System.out.println("Window is already close");
            return;
        }
        isLocked = locked;
    }


}

