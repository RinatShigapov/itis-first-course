package firstSemester.task1;

public class Main {
    public static void main(String[] args) {

        Triangle t = new Triangle(100,50);
        System.out.println(t.getArea());


        Rectangle r = new Rectangle(100, 50);
        System.out.println(r.getArea() + " " + r.getPerimetr());

        Circle c = new Circle(5);
        System.out.println(c.getRadius() + " " + c.getArea() + " " + c.getLength());

        Parallelepiped p = new Parallelepiped(10, 20, 30);
        System.out.println(p.getArea() + " " + p.getPerimetr() + " " + p.getVolume());


    }
}
