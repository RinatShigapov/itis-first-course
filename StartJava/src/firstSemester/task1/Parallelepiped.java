package firstSemester.task1;

public class Parallelepiped {
    private int length;
    private int width;
    private int heigt;

    public Parallelepiped(int length, int width, int heigt) {
        this.length = length;
        this.width = width;
        this.heigt = heigt;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeigt() {
        return heigt;
    }

    public void setHeigt(int heigt) {
        this.heigt = heigt;
    }

    public int getVolume(){
        return length*width*heigt;
    }

    public int getPerimetr(){
        return (length+width+heigt)*2;
    }

    public int getArea(){
        return (length*width+width*heigt+heigt*length)*2;
    }

}
