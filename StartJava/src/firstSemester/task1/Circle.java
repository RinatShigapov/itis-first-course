package firstSemester.task1;

public class Circle {
    final double Pi = 3.14;
    private int radius;

    public Circle(int radius) {
        this.radius = radius;
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }
    public int getArea(){
        return (int)(Pi*radius*radius);
    }

    public int getLength(){
        return (int)(2*Pi*radius);
    }

}
