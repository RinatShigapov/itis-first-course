package firstSemester.tasks;

import java.util.Scanner;

public class Task016 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Write n");
        int n = sc.nextInt();
        System.out.println("Write x");
        double x = sc.nextDouble();
        double s = 0;
        double d;
        for (int i = 1; i < n + 1; i++) {
            d = 1;
            for (int j = 1; j < i + 1; j++) {
                d = d * (x + j);
            }
            s = s + d;
        }
        System.out.println(s);
    }
}
