package firstSemester.tasks;

public class Task006 {
    public static void main(String[] args) {
        int result;
        int x = 1;
        //101 + x*(30 + x*(25 + x*(10 + x*(6 + x))))
        result = x + 6;
        result *= x;
        result += 10;
        result *= x;
        result += 25;
        result *= x;
        result += 30;
        result *= x;
        result += 101;
        System.out.println(result);
    }
}
