package firstSemester.tasks;

import java.util.Scanner;

public class Task018 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        double res = 0;
        double r;
        double a;
        System.out.println("Write x");
        double x = sc.nextDouble();
        System.out.println("Write n");
        int n = sc.nextInt();
        for (int i = n; i >= 0; i--) {
            System.out.println("Write a" + i);
            a = sc.nextDouble();
            r = a*Math.pow(x,i);
            res += r;
        }
        System.out.println(res);
    }
}
