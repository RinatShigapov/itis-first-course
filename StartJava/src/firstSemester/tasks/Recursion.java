package firstSemester.tasks;

public class Recursion {
    int factarial(int n){
        int result;

        if (n == 1){
            return 1;
        }
        result = factarial(n-1) * n;
        return result;
    }

    public static void main(String[] args) {
        Recursion r = new Recursion();
        System.out.println(r.factarial(6));
    }
}
