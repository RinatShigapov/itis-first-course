package firstSemester.tasks;

import java.util.Scanner;

public class Task011 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int p = 1;
        while (n > 0) {
            p *= n;
            n -= 2;
        }
        System.out.println(p);
    }
}
