package firstSemester.tasks;

import java.util.Scanner;

public class Task017 {
    public static void main(String[] args) {
        Recursion r = new Recursion();
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        double a, b;
        double s = 0;
        double res;
        for (int m = 2; m < n + 1; m++) {
            a = r.factarial(m - 1);
            b = r.factarial(2 * m);
            res = (a * a / b);
            s += res;
        }
        System.out.println(s);
    }
}
