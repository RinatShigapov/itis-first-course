package firstSemester.tasks;

import java.util.Scanner;

public class Task014 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Write n");
        int n = sc.nextInt();
        System.out.println("Write x");
        int x = sc.nextInt();
        double cos = 0;
        while (n>0){
            cos = Math.cos(cos + x);
            n--;
        }
        System.out.println(cos);
    }
}
