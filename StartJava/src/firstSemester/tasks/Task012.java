package firstSemester.tasks;

import java.util.Scanner;

public class Task012 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        double s = 1;
        double d = 3.0;
        for (int i = 0; i < n; i++) {
            if (i / 2 == 0) {
                System.out.println((1 / (d * d)));
                s = s - (1 / (d * d));
            } else {
                System.out.println((1 / (d * d)));
                s = s + (1 / (d * d));
            }
            d += 2;
        }
        System.out.println(s);
    }
}
