package firstSemester.tasks;

public class Task008 {
    public static void main(String[] args) {
        int k = 3;
        for (int i = 2; i < 10; i++){
            System.out.println(i + " * " + k + " = " + i*k);
        }
    }
}
