package firstSemester.tasks;

public class Task009 {
    public static void main(String[] args) {
        int x = 5;
        double y;
        if (x > 2) {
            y = (double) (x * x - 1) / (x + 2);
        } else if (x > 0) {
            y = (x * x - 1) * (x + 2);
        } else {
            y = x * x * (1 + 2 * x);
        }
        System.out.println(y);
    }
}
