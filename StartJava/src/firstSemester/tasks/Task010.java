package firstSemester.tasks;

import java.util.Scanner;

public class Task010 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int x = sc.nextInt();
        double y = x > 2 ? (double) (x * x - 1) / (x + 2) : (x > 0 ? (x * x - 1) * (x + 2) : x * x * (1 + 2 * x));
        System.out.println(y);
    }
}
