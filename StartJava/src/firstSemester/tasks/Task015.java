package firstSemester.tasks;

import java.util.Scanner;

public class Task015 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Write n");
        int n = sc.nextInt();
        System.out.println("Write x");
        double x = sc.nextDouble();
        double s = n + x;
        for (int i = n - 1; i > 0; i--) {
            s = i + x / s;
        }
        System.out.println(s);
    }
}
