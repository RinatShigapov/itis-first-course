package firstSemester.tasks;

import java.util.Scanner;

public class Task013 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        double r = 1;
        for (int i = 1; i < n + 1; i++) {
            r = r * (4 * i * i)/((2*i-1)*(2*i+1));
        }
        System.out.println(r);
    }
}
