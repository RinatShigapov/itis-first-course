package firstSemester.task15;

public class OperationsArrays {
    public static void join(char[][] a1, char[][] a2){
        char[][] join = new char[a1.length][a1[0].length];
        if (a1.length == a2.length && a1[0].length == a2[0].length) {
            for (int i = 0; i < join.length; i++) {
                for (int j = 0; j < join[0].length; j++) {
                    if ((a1[i][j] == '*') ||  (a2[i][j] == '*')){
                        join[i][j] = '*';
                    }else join[i][j] = '0';
                }
            }
            for (int i = 0; i < join.length; i++) {
                for (int j = 0; j < join[0].length; j++) {
                    System.out.print(join[i][j] + " ");
                }
                System.out.println();
            }
        }else{
            System.out.println("Arrays don't equals");
        }
    }

    public static void intersect(char[][] a1, char[][] a2){
        char[][] intersect = new char[a1.length][a1[0].length];
        if (a1.length == a2.length && a1[0].length == a2[0].length) {
            for (int i = 0; i < intersect.length; i++) {
                for (int j = 0; j < intersect[0].length; j++) {
                    if ((a1[i][j] == '*') &&  (a2[i][j] == '*')){
                        intersect[i][j] = '*';
                    }else intersect[i][j] = '0';
                }
            }
            for (int i = 0; i < intersect.length; i++) {
                for (int j = 0; j < intersect[0].length; j++) {
                    System.out.print(intersect[i][j] + " ");
                }
                System.out.println();
            }
        }else{
            System.out.println("Arrays don't equals");
        }
    }

    public static void main(String[] args) {
        char[][] a1 = {{' ', '*', ' '},
                {' ', '*', ' '},
                {' ', '*', ' '}};
        char[][] a2 = {{' ', ' ', ' '},
                {'*', '*', '*'},
                {' ', ' ', ' '}};

        System.out.println("join:");
        join(a1,a2);
        System.out.println("intersect:");
        intersect(a1, a2);
    }
}
