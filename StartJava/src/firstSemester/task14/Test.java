package firstSemester.task14;

public class Test {
    public static void main(String[] args) {
        Box<String> box = new Box<>();
        box.setItem("hello");

        Box<Box<String>> box1 = new Box<>();
        box1.setItem(box);

        Box<Box<Box<String>>> box2 = new Box<>();
        box2.setItem(box1);

        String str = box.getItem();
        System.out.println(box2.getItem().getItem().getItem());
        System.out.println(str);

        Box2<String, Integer> box22= new Box2<>(str, 435);
    }
}
