package firstSemester.Homework2;

public class Example {
    public static void main(String[] args) {
        String str = "hello";
        int max = str.length();
        int vowel = 0;
        int i = 0;
        while (i <= max - 1){
            char c = str.charAt(i);
            switch (c){
                case 'a':
                case 'e':
                case 'i':
                case 'o':
                case 'u':
                    vowel++;
                    break;
            }
            i++;
        }
        System.out.println(vowel);
    }
}
