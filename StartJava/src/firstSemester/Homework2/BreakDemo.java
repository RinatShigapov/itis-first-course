package firstSemester.Homework2;

public class BreakDemo {
    public static void main(String[] args) {

        int[] arrayOfInts = { 32, 87, 3, 589, 12,
                1076, 2000, 8, 622, 127};
        int seqrchfor = 12;
        int i;
        boolean foundIt = false;

        for (i = 0; i < arrayOfInts.length; i++){
            if (arrayOfInts[i] == seqrchfor){
                foundIt = true;
                break;
            }
        }

        if (foundIt){
            System.out.println("Found " + seqrchfor + " at index " + i);
        } else {
            System.out.println(seqrchfor + " not in the array");
        }
    }
}
