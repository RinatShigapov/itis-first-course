package firstSemester.task7;

public class Task6 {
    public static void anagram(String[] args) {
        if (args[0].length() == args[1].length()) {
            StringBuffer s = new StringBuffer(args[1]);

            for (int i = 0; i < args[0].length(); i++) {
                for (int j = 0; j < s.length(); j++) {
                    if (args[0].charAt(i) == s.charAt(j)) {
                        s.deleteCharAt(j);
                        break;
                    }
                }
            }
            if (s.length() == 0) {
                System.out.println("true");
            } else {
                System.out.println("false");
            }

        } else {
            System.out.println("false");

        }
    }

    public static void main(String[] args) {
        anagram(args);
    }
}
