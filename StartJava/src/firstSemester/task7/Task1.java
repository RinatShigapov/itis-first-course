package firstSemester.task7;

public class Task1 {
    public static void main(String[] args) {
        System.out.printf("Byte: min %d max %d%n " +
                "Short: min %d max %d%n" +
                "Integer: min %d max %d%n" +
                "Long: min %d max %d%n" +
                "Float: min %.5f max %.5f%n" +
                "Double: min %.5f max %.5f%n",
                Byte.MIN_VALUE, Byte.MAX_VALUE,
                Short.MIN_VALUE, Short.MAX_VALUE,
                Integer.MIN_VALUE, Integer.MAX_VALUE,
                Long.MIN_VALUE, Long.MAX_VALUE,
                Float.MIN_VALUE, Float.MAX_VALUE,
                Double.MIN_VALUE, Double.MAX_VALUE);

    }
}
