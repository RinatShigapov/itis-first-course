package firstSemester.task7;

public class Adder {
    public static void adder(String args[]) {
        int res = 0;
        for (int i = 0; i < args.length; i++) {
            res += Integer.parseInt(args[i]);

        }
        if (args.length > 1) {
            System.out.println(res);
        } else {
            System.out.println("ERROR");
        }
    }

    public static void main(String[] args) {
        adder(args);
    }
}
