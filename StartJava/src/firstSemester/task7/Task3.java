package firstSemester.task7;

public class Task3 {
    public static void adder(String args[]) {
        double res = 0;
        for (int i = 0; i < args.length; i++) {
            res += Double.parseDouble(args[i]);

        }
        res = Math.round(res * 100);
        res /= 100f;
        if (args.length > 1) {
            System.out.println(res);
        } else {
            System.out.println("ERROR");
        }
    }

    public static void main(String[] args) {
        adder(args);
    }
}

