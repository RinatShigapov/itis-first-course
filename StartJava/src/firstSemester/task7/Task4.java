package firstSemester.task7;

public class Task4 {
    public static void calculator(String[] args) {
        int a = Integer.parseInt(args[0]);
        int b = Integer.parseInt(args[2]);
        int res = 0;
        switch (args[1]) {
            case "+":
                res = a + b;
                break;

            case "-":
                res = a - b;
                break;

            case "*":
                res = a * b;
                break;
        }
        System.out.println(res);

    }

    public static void main(String[] args) {
        calculator(args);
    }

}
