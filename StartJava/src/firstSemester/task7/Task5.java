package firstSemester.task7;

public class Task5 {
    public static void NameInitials(String[] args) {
        String initials;
        if (args.length == 3) {
            initials = args[0].charAt(0) + ". " + args[1].charAt(0) + ". " + args[2].charAt(0) + ".";
        } else {
            initials = args[0].charAt(0) + ". " + args[1].charAt(0) + ".";
        }
        System.out.println(initials);
    }

    public static void main(String[] args) {
        NameInitials(args);
    }
}
