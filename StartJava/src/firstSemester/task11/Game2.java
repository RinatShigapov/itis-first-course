package firstSemester.task11;

import java.util.Scanner;

public class Game2 {

    public static void ticTacToe() {
        String[][] table = new String[3][3];
        String first = "  a b c";
        Scanner sc = new Scanner(System.in);
        String move;
        String symbol;
        int number = 1;
        int l = 2;
        int r = 2;
        boolean win = false;
        boolean freePlace = true;


        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                table[i][j] = " ";
            }
        }

        System.out.println(first);
        for (int i = 0; i < 3; i++) {
            System.out.println(i + 1 + " " + table[i][0] + " " + table[i][1] + " " + table[i][2]);
        }

        while (freePlace && !win) {
            freePlace = false;
            System.out.println("First write number, then letter ");
            if (number % 2 == 0) {
                symbol = "O";
                System.out.println("Player 2: ");
            } else {
                symbol = "X";
                System.out.println("Player 1: ");
            }
            move = sc.nextLine();
            switch (move) {
                case "1a":
                    l = 0;
                    r = 0;
                    break;
                case "1b":
                    l = 0;
                    r = 1;
                    break;
                case "1c":
                    l = 0;
                    r = 2;
                    break;
                case "2a":
                    l = 1;
                    r = 0;
                    break;
                case "2b":
                    l = 1;
                    r = 1;
                    break;
                case "2c":
                    l = 1;
                    r = 2;
                    break;
                case "3a":
                    l = 2;
                    r = 0;
                    break;
                case "3b":
                    l = 2;
                    r = 1;
                    break;
                case "3c":
                    l = 2;
                    r = 2;
                    break;
                default:
                    System.out.println("This element not found. Repeat again.");
            }

            if (table[l][r].equals(" ")) {
                table[l][r] = symbol;
                number++;
                System.out.println(first);
                for (int t = 0; t < 3; t++) {
                    System.out.println(t + 1 + " " + table[t][0] + " " + table[t][1] + " " + table[t][2]);
                }
            } else {
                System.out.println("This position is occupied. Repeat again.");
            }

            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < 3; j++) {
                    if (table[i][j].equals(" ")) {
                        freePlace = true;
                    }
                }
                if (table[i][0].equals(table[i][1]) && table[i][0].equals(table[i][2]) && !(table[i][0].equals(" ")) && !(table[i][0].equals(" ")) || (table[0][i].equals(table[1][i]) && table[0][i].equals(table[2][i])&& !(table[0][i].equals(" ")))) {
                    win = true;
                }
            }

            if ((table[1][1].equals(table[0][0]) && table[1][1].equals(table[2][2]) || (table[1][1].equals(table[0][2]) && table[1][1].equals(table[2][0]))) && !(table[1][1].equals(" "))) {
                win = true;
            }
        }

        if (!(freePlace) && !(win)) {
            System.out.println("Draw");
        }
        if (win){
            if(number%2 == 0){
                System.out.println("Player 1 win");
            } else {
                System.out.println("Player 2 win");
            }

        }
    }

    public static void main(String[] args) {
        ticTacToe();
    }

}