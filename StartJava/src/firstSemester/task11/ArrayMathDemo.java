package firstSemester.task11;

public class ArrayMathDemo {

    public static int sum(int[] b) {
        int sum = 0;
        for (int i : b) {
            sum += i;
        }
        return sum;
    }

    public static double arg(int[] b) {
        double arg;
        int sum = 0;
        for (int i : b) {
            sum += i;
        }
        double s = (double)(sum);
        arg = s/ b.length;
        return arg;
    }

    public static int min(int[] b) {
        int min = b[0];
        for (int i = 1; i < b.length; i++) {
            if (b[i] < min) {
                min = b[i];
            }
        }
        return min;
    }

    public static int max(int[] b) {
        int max = b[0];
        for (int i = 1; i < b.length; i++) {
            if (b[i] > max) {
                max = b[i];
            }
        }
        return max;
    }


    public static void main(String[] args) {
        int[] a = {5, 7, 8, 10, 15, 2, 3};
        System.out.println("Sum: " + sum(a));
        System.out.println("Arg: " + arg(a));
        System.out.println("Min: " + min(a));
        System.out.println("Max: " + max(a));
    }
}
