package firstSemester.task11;

import java.util.Scanner;

public class Game3 {
    public static void rockPaperScissors() {
        Scanner sc = new Scanner(System.in);
        int random = (int) (1 + Math.random() * 3);

        System.out.println("rock, paper, scissors");
        switch (sc.nextLine()) {
            case "rock":
                switch (random) {
                    case 1:
                        System.out.println("rock \nDraw");
                        break;
                    case 2:
                        System.out.println("paper \nYou lose!");
                        break;
                    case 3:
                        System.out.println("scissors \nYou win!");
                        break;
                }
                break;

            case "paper":
                switch (random) {
                    case 1:
                        System.out.println("rock \nYou win!");
                        break;
                    case 2:
                        System.out.println("paper \nDraw");
                        break;
                    case 3:
                        System.out.println("scissors \nYou lose!");
                        break;
                }
                break;

            case "scissors":
                switch (random) {
                    case 1:
                        System.out.println("rock \nYou lose!");
                        break;
                    case 2:
                        System.out.println("paper \nYou win!");
                        break;
                    case 3:
                        System.out.println("scissors \nDraw");
                        break;
                }
                break;

            default:
                System.out.println("This element did't find.");

        }
    }

    public static void main(String[] args) {
        rockPaperScissors();
    }

}
