package firstSemester.Homework1;

public class instanceofDemo {
    public static void main(String[] args) {
        Parent obj1 = new Parent();
        Parent obj2 = new Child();

        System.out.println( "obj1 instanceof firstSemester.Homework1.Parent: "
                + (obj1 instanceof Parent));
        System.out.println( "obj1 instanceof firstSemester.Homework1.Child: "
                + (obj1 instanceof Child));
        System.out.println( "obj1 instanceof firstSemester.Homework1.MyInterface: "
                + (obj1 instanceof MyInterface));
        System.out.println( "obj2 instanceof firstSemester.Homework1.Parent: "
                + (obj2 instanceof Parent));
        System.out.println( "obj2 instanceof firstSemester.Homework1.Child: "
                + (obj2 instanceof Child));
        System.out.println( "obj2 instanceof firstSemester.Homework1.MyInterface: "
                + (obj2 instanceof MyInterface));
    }
}

class Parent {}
class Child extends Parent implements MyInterface {}
interface MyInterface {}