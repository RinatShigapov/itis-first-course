package firstSemester.exam;

public class Schoolboy extends Human{
    private int form;

    public Schoolboy(String position, int age, int rating, int form) {
        super(position, age, rating);
        this.form = form;
    }

    @Override
    public void studying() {
        System.out.println("Пришел на занятия в школу");
    }
}
