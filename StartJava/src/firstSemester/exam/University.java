package firstSemester.exam;

public class University implements Institution{
    private String name;


    private int sumStudents;

    public University(String name, String director) {
        this.name = name;
        this.director = director;
    }

    private String director;

    @Override
    public void training() {
        System.out.println("Здесь учатся студенты");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSumStudents() {
        return sumStudents;
    }

    public void setSumStudents(int sumStudents) {
        this.sumStudents = sumStudents;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

}
