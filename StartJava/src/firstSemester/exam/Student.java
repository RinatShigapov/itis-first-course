package firstSemester.exam;

public class Student extends Human {
    private int course;

    public int getCourse() {
        return course;
    }

    public void setCourse(int course) {
        this.course = course;
    }

    public University getUniversity() {
        return university;
    }

    public void setUniversity(University university) {
        this.university = university;
    }

    private University university;

    public Student(String position, int age, int rating,int course, University university) {
        super(position, age, rating);
        this.course = course;
        this.university = university;
    }

    @Override
    public void studying() {
        System.out.println("Пришел на занятия в универ");
    }

}
