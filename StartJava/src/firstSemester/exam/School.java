package firstSemester.exam;

public class School implements Institution{
    private String name;
    private int sumSchoolboy;
    private String director;

    public School(String name, String director) {
        this.name = name;
        this.director = director;
    }

    @Override
    public void training() {
        System.out.println("Здесь учаться школьники");
    }
}
