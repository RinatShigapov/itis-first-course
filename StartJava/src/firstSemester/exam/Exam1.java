package firstSemester.exam;

public class Exam1 {
    public static void main(String[] args) {
        int n = 4;
        int[] a = {4, 6, 4, 4};
        if (mod2(sumDigitsNumber(n)) && quadEq(a)) {
            System.out.println("верно");
        } else {
            System.out.println("неверно");
        }

    }

    /* public static int sumDigitsArray(int[] a){
         int s = 0;
         for (int i = 0; i < a.length; i++) {
             s += sumDigitsNumber(a[i]);
         }
         return s;
     }
 */
    public static int sumDigitsNumber(int i) {
        int s = 0;
        int a = i;
        int b;
        while (a > 0) {
            s += a % 10;
            a = a / 10;
        }
        return s;
    }

    public static boolean mod2(int s) {
        int a = s;
        boolean res = false;
        while (a > 0) {
            if ((a % 10) % 2 == 0) {
                res = true;
                break;
            }
        }
        return res;
    }

    public static boolean quadEq(int[] a) {

        for (int i = 0; i < a.length; i++) {
            if ((2 * i + fact(i)) >= 4 * fact(a[i])) {
                return true;

            }
        }
        return false;
    }

    public static int fact(int i) {
        if (i == 0) {
            return 1;
        }
        int result = i * fact(i - 1);
        return i;
    }


}
