package firstSemester.exam;

public abstract class Human {
    protected String position;
    protected int age;
    protected int rating;
    protected String surname;

    public Human(String position, int rating) {
        this.position = position;
        this.rating = rating;
    }

    public Human(String position, int age, int rating) {
        this.position = position;
        this.age = age;
        this.rating = rating;
    }

    public abstract void studying();
    public void stayingInHome(){
        System.out.println("Я останусь дома вместо занятий");
    }
}
