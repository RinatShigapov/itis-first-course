package firstSemester.map;

/**
 * test and doing firstSemester.map
 */
public class MapDemo {
    public static void main(String[] args) {
        MapArea mp = new MapArea(8, 6, ' ');
        mp.placeObject(2, 3, '!');

        MapDisplay md = new MapDisplay();
        //md.display(mp);

        MapCoords mc = new MapCoords();
        mc.convert("a3");
        mp.placeObject(mc.getY(), mc.getX(), '*');
        System.out.println(mc.getX() + " " + mc.getY());
        //md.display(mp);

        Cell cell = new Cell(2,3, mp);

        Moving mv = new Moving(mp, cell);
        md.display(mp);
        mv.move("d");
        md.display(mp);
    }
}
