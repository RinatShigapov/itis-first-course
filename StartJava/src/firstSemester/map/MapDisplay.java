package firstSemester.map;

/**
 * displays firstSemester.map
 */
public class MapDisplay {

    /**
     * doing firstSemester.map with coords
     * @param map
     */
    public void display(MapArea map) {
        char a = 97;
        for (int i = 0; i <= map.getHeight(); i++) {
            for (int j = 0; j <= map.getWidth(); j++) {
                if ((i == 0) && (j == 0)) {
                    System.out.print(" ");
                } else if (i == 0) {
                    System.out.print(" " + a);
                    a++;
                } else if (j == 0) {
                    System.out.print(i);
                } else {
                    System.out.print(" " + map.getCell(i, j));
                }
            }
            System.out.println();
        }
    }
}
