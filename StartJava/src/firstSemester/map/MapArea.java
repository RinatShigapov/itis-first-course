package firstSemester.map;

/**
 * create array(area) and perform some operations with it
 * @see MapDemo
 * @author Rinat
 */
public class MapArea {
    private int width;
    private int height;
    private char start;
    private char [][] area;

    /**
     *
     * @param width
     * @param height
     * @param start what's symbol fill array (default ' ')
     */
    public MapArea(int width, int height, char start) {
        this.width = width;
        this.height = height;
        area = new char[height][width];
        this.start = start;
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                area[i][j] = start;
            }
        }
    }

    /**
     * place object with this coords
     * @param x width
     * @param y height
     * @param o object
     */
    public void placeObject(int y, int x, char o) {
        area[y-1][x-1] = o;
    }

    /**
     * @param y
     * @param x
     * @return object in cell[y][x]
     */
    public char getCell(int y, int x){
        return area[y-1][x-1];
    }

    /**
     * @return width area
     */
    public int getWidth() {
        return width;
    }

    /**
     * @return height area
     */
    public int getHeight() {
        return height;
    }

    /**
     *
     * @return default element
     */
    public char getChar() {
        return start;
    }



}
