package firstSemester.map;

/**
 * converts string to coords
 */
public class MapCoords {
    private int y;
    private int x;

    /**
     * @param coords letter and number
     * converts string to coords for array
     */
    public void convert(String coords) {
        for (int i = 0; i < coords.length(); i++) {
            if (coords.charAt(i) >= 97 && coords.charAt(i) <= 122) {
                x = (coords.charAt(i) - 'a' + 1);
            } else if (coords.charAt(i) >= 48 && coords.charAt(i) <= 57) {
                if (coords.length() > 2) {
                    int y1 = coords.charAt(i) - '0';
                    if (y == 0) {
                        y1 = y1 * 10;
                    }
                    y += y1;
                } else {
                    y = coords.charAt(i) - '0';
                }
            } else {
                System.out.println("This cell didn't found.");
            }
        }
    }

    /**
     * @return y
     */
    public int getY() {
        return y;
    }

    /**
     * @return x
     */
    public int getX() {
        return x;
    }

}
