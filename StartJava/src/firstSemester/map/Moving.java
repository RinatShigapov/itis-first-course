package firstSemester.map;

/**
 * move object around the firstSemester.map
 */
public class Moving {
    private MapArea map;
    private Cell cell;
    private int x;
    private int y;

    /**
     * @param map what firstSemester.map
     * @param c what cell you choose
     */
    public Moving(MapArea map, Cell c) {
        this.map = map;
        cell = c;
        y = c.getY();
        x = c.getX();
    }

    /**
     * move element
     * @param move
     */
    public void move(String move) {
        if (!cell.isEmpty(y, x)) {
            try {
                switch (move) {
                    case "w":
                        y += Direction.UP.getY();
                        break;
                    case "s":
                        y += Direction.DOWN.getY();
                        break;
                    case "a":
                        x += Direction.LEFT.getX();
                        break;
                    case "d":
                        x += Direction.RIGHT.getX();
                        break;
                    default:
                        System.out.println("This word didn't found.");
                }
                map.placeObject(y, x, cell.getObject());
                cell.deleteObject();
            } catch (ArrayIndexOutOfBoundsException exception) {
                if (y == map.getHeight() + 1) {
                    y = map.getHeight();
                } else if (y == 0) {
                    y = 1;
                } else if (x == map.getWidth() + 1) {
                    x = map.getWidth();
                } else if (x == 0) {
                    x = 1;
                }
                System.out.println("You can not go there, because you're at the end of the firstSemester.map.\n");
            }
        } else System.out.println("This cell is empty.");
        cell.setX(x);
        cell.setY(y);
    }

}
