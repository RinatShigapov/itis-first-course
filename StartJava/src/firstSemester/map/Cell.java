package firstSemester.map;

/**
 * Give information about cell
 */
public class Cell {
    private int x;
    private int y;
    MapArea area;

    /**
     * @param y coord
     * @param x coord
     * @param area where
     */
    public Cell(int y, int x, MapArea area) {
        this.x = x;
        this.y = y;
        this.area = area;
    }

    /**
     * @param x coords
     * @param y coords
     * @return information about empty
     */
    public boolean isEmpty(int y, int x) {
        if (area.getCell(y, x) == area.getChar()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @return object
     */
    public char getObject() {
        return area.getCell(y, x);
    }

    /**
     * @return cell x
     */
    public int getX() {
        return x;
    }

    /**
     * change x
     */
    public void setX(int x) {
        this.x = x;
    }

    /**
     * @return cell x
     */
    public int getY() {
        return y;
    }

    /**
     * change x
     */
    public void setY(int y) {
        this.y = y;
    }

    /**
     * delete object in cell[y][x]
     */
    public void deleteObject() {
        area.placeObject(y, x, area.getChar());
    }

}
