package firstSemester.task4;

public class Room {
    private Window w;
    private Door d;
    private Table t;


    public Room(Window w, Door d) {
        this.w = w;
        this.d = d;
    }

    public void closeRoom() {
        d.closeDoor();
    }

    public void openRoom() {
        d.openDoor();
    }

    public void hasTable(Table t) {
        if (t.isInRoom() == true) {
            System.out.println("Table is in the room");
        } else {
            System.out.println("Table is not in the room");
        }
    }

    public void coldWind() {
        if ((d.isLockedDoor() == false) && (w.isLockedWindow() == false)) {
            System.out.println("Cold wind is in the room."); //Сквозняк в комнате
        } else {
            System.out.println("The room is warm."); //Сквозняка в комнате нет
        }
    }

    public void specifications(Table t, Room r) {
        System.out.println("Door is closed = " + d.isLockedDoor() + "\n" + "Window is closed = " + w.isLockedWindow() + "\n" + " Table is has in room = " + t.isInRoom());
    }
}
