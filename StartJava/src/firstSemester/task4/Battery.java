package firstSemester.task4;

public class Battery {
    public Battery(int capacity) {
        this.capacity = capacity;
    }

    public int getCapacity() {
        return capacity;
    }

    private int capacity = 10;
    public int decrease(){
        return --capacity;

    }
}
