package firstSemester.task4;

public class Door {
    private boolean lockedDoor;

    public boolean isLockedDoor() {
        return lockedDoor;
    }

    public void openDoor() {
        if (lockedDoor == true) {
            lockedDoor = false;
            System.out.println("Door open");
        } else {
            System.out.println("Door already opened");
        }
    }


    public void closeDoor() {
        if (lockedDoor == false) {
            lockedDoor = true;
            System.out.println("Door close");
        } else {
            System.out.println("Door already closed");
        }
    }
}
