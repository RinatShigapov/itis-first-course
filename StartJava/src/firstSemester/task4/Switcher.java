package firstSemester.task4;

public class Switcher {

    private Lamp l;

    public Switcher(Lamp l) {
        this.l = l;
    }

    public boolean switchOnOff(){
        return l.isTurnOn();
    }

}
