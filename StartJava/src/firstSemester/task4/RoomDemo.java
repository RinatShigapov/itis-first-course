package firstSemester.task4;

public class RoomDemo {
    public static void main(String[] args) {
        Window w = new Window();
        Door d = new Door();
        Table t = new Table();
        Room r = new Room(w, d);
        r.openRoom();
        r.closeRoom();
        r.closeRoom();
        w.closeWindow();
        w.openWindow(d);
        r.coldWind();
        d.closeDoor();
        r.specifications(t, r);
        r.coldWind();
        t.moveTable();
        r.hasTable(t);
        r.specifications(t, r);
    }
}
