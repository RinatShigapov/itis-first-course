package firstSemester.task4;

public class Window {

    private boolean lockedWindow = true;

    public boolean isLockedWindow() {
        return lockedWindow;
    }

    public void openWindow(Door d) {
        if (lockedWindow == true) {
            lockedWindow = false;
            System.out.println("Window open");
            hasDraft(d);
        } else {
            System.out.println("Window already opened ");
        }
    }


    public void closeWindow() {
        if (lockedWindow == false) {
            lockedWindow = true;
            System.out.println("Window close");
        } else {
            System.out.println("Window already closed");
        }
    }

    public void hasDraft(Door d) {
        d.openDoor();
        System.out.println("There is a draft in the room. Door opened."); //Сквозняк в комнате
    }

}
