package firstSemester.task4;

public class WatchDemo {
    public static void main(String[] args) {
        Battery b = new Battery(10);
        Watch w = new Watch(b);
        for (int i = 0; i < 15; i++) {
            w.tick();
        }
    }
}
