package firstSemester.task4;

public class Watch {
    private Battery a;

    public Watch(Battery a) {
        this.a = a;
    }

    public void tick() {
        if (a.getCapacity() > 0) {
            a.decrease();
            System.out.println("tick");
        } else {
            System.out.println("Died");
        }
    }
}
