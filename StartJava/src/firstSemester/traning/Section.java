package firstSemester.traning;

public class Section {
    private int startX;
    private int endX;
    private int startY;
    private int endY;


    public Section(int startX, int endX, int startY, int endY) {
        this.startX = startX;
        this.endX = endX;
        this.startY = startY;
        this.endY = endY;
    }

    public double getLength(){
        double l = Math.sqrt((endY-endX)*(endY-endX)+ (endY-endX)*(endY-endX));

        return l;
    }
}
