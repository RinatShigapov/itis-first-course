package firstSemester.traning;

public class Ex1 {
    public static void main(String[] args) {

        String[] str = {"avda", "adsw", "fwob"};
        comparison(str);
    }

    public static boolean vowel(char c) {
        switch (c) {
            case 'a':
            case 'e':
            case 'i':
            case 'o':
            case 'u':
            case 'y':
                return true;
            default:
                return false;
        }
    }

    public static void comparison(String[] str) {
        boolean result = false;
        boolean comparison = false;
        int n = str.length;

        for (int i = 0; i < n - 1; i++) {
            String s1 = str[i];
            String s2 = str[i + 1];
            int min = str[i].length() <= str[i + 1].length() ? str[i].length() : str[i + 1].length();
            for (int j = 0; j < min; j += 2) {
                if (vowel(s1.charAt(j)) == vowel(s2.charAt(j))) {
                    comparison = true;
                } else {
                    comparison = false;
                    break;
                }
            }
            if (comparison) {
                result = true;
                break;
            }
        }
        System.out.println(result);
    }
}
