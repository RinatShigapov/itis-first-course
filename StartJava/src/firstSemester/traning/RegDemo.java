package firstSemester.traning;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegDemo {
    public static void main(String[] args) {
        String str = new String("Камилла, Даниил, Камила, Данила");
        Pattern p = Pattern.compile("(Камилл?а)|(Дани((ил)|(ла)))");
        Matcher matcher = p.matcher(str);
        while (matcher.find()) {
            int start = matcher.start();
            int end = matcher.end();
            System.out.println("Найдено совпадение " + str.substring(start, end) + " с " + start + " по " + (end - 1) + " позицию");
        }

        byte b = 100;
        byte a = (byte) (b & (b + 10));
        byte c = (byte)(a >> 2);
        System.out.println(c);
        byte d = 17;
        System.out.println(d >> 1);

        String str1 = new String("Регина, Азат, Эвелина, Азамат");
        Pattern p1 = Pattern.compile("(Аза(ма)?т)|((Рег)|(Эвел)ина)");
        Matcher matcher1 = p1.matcher(str1);
        while (matcher1.find()) {
            int start = matcher1.start();
            int end = matcher1.end();
            System.out.println("Найдено совпадение " + str1.substring(start, end) + " с " + start + " по " + (end - 1) + " позицию");
        }
    }
}
