package firstSemester.traning;

public class castObj {
    public static class A{
        public void method(){
            System.out.println("Реализация метода родителя");
        }
    }

    public static class B extends A{
        @Override
        public void method(){
            System.out.println("Реализация метода потомка");
        }
        public void anotherMethod(){
            System.out.println("Доп метод");
        }
    }

    public static void main(String[] args) {
        A ex = new B();
        ex.method();
        ((B)ex).anotherMethod();
    }
}
