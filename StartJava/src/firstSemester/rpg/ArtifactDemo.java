package firstSemester.rpg;

public class ArtifactDemo {
    public static void main(String[] args) {
        Character c = new Character("user1");
        Artifact weapon = new Artifact("weapon", 0, 5, 0.2, 0);
        Artifact helmet = new Artifact("helmet", 20, 0, 0, 2);
        Artifact chestplate = new Artifact("chestplate", 30, 0, 0, 3);
        Artifact boots = new Artifact("boots", 10, 0, 0, 1);
        Artifact shield = new Artifact("shield", 20, 0, 0, 2);
        c.addArtifact(weapon);
        c.info();
        c.addArtifact(helmet);
        c.info();
        c.addArtifact(chestplate);
        c.addArtifact(boots);
        c.addArtifact(shield);
        c.info();
        Artifact weapon2 = new Artifact("weapon", 0, 3, 0.1, 0);
        c.addArtifact(weapon2);
        c.info();

    }

}
