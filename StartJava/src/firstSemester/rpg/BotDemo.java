package firstSemester.rpg;

public class BotDemo {
    public static void main(String[] args) {
        Artifact sword = new Artifact("weapon",0,5,0.2,0);
        Artifact helmet = new Artifact("helmet", 40,0,0, 5);
        Bot b = new Bot("Bot", sword);
        Character c = new Character("Player");
        c.addArtifact(helmet);

        while (b.isAlive() && c.isAlive()) {


            int hit1 = b.hit();
            boolean agil2 = c.isAgility();

            if (!agil2) {
                System.out.printf("%s (%d) hits %s (%d) - %d ", b.getName(), b.getHealth(), c.getName(), c.getHealth(), hit1);
                if (hit1 > b.getMaxHit()) {
                    System.out.println("(crit)");
                } else if (b.isAlive() && hit1 == 0) {
                    System.out.println("(anticrit)");
                } else {
                    System.out.println();
                }
                c.getHit(hit1);
            } else {
                System.out.printf("%s (%d) tried to hit %s (%d) (missed) \n", b.getName(), b.getHealth(), c.getName(), c.getHealth());
            }


            int hit2 = c.hit();
            boolean agil1 = b.isAgility();

            if (!agil1) {
                System.out.printf("%s (%d) hits %s (%d) - %d ", c.getName(), c.getHealth(), b.getName(), b.getHealth(), hit2);
                if (hit2 > c.getMaxHit()) {
                    System.out.println("(crit)\n");
                } else if (c.isAlive() && hit2 == 0) {
                    System.out.println("(anticrit)\n");
                } else {
                    System.out.println("\n");
                }
                b.getHit(hit2);
            } else {
                System.out.printf("%s (%d) tried to hit %s (%d) (missed) \n\n", c.getName(), c.getHealth(), b.getName(), b.getHealth());
            }


        }

        if (b.isDead()) {
            System.out.println(b.getName() + " is dead");
            b.dropArtifact(sword);
        } else if (c.isDead()) {
            System.out.println(c.getName() + " is dead");
        }
    }
}
