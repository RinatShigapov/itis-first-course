package firstSemester.rpg;

public class BattleDemo {

    public static void main(String[] args) {
        Character char1 = new Character("Player1");
        Character char2 = new Character("Player2");


        while (char1.isAlive() && char2.isAlive()) {


            int hit1 = char1.hit();
            boolean agil2 = char2.isAgility();

            if (!agil2) {
                System.out.printf("%s (%d) hits %s (%d) - %d ", char1.getName(), char1.getHealth(), char2.getName(), char2.getHealth(), hit1);
                if (hit1 > char1.getMaxHit()) {
                    System.out.println("(crit)");
                } else if (char1.isAlive() && hit1 == 0) {
                    System.out.println("(anticrit)");
                } else {
                    System.out.println();
                }
                char2.getHit(hit1);
            } else {
                System.out.printf("%s (%d) tried to hit %s (%d) (missed) \n", char1.getName(), char1.getHealth(), char2.getName(), char2.getHealth());
            }


            int hit2 = char2.hit();
            boolean agil1 = char1.isAgility();

            if (!agil1) {
                System.out.printf("%s (%d) hits %s (%d) - %d ", char2.getName(), char2.getHealth(), char1.getName(), char1.getHealth(), hit2);
                if (hit2 > char2.getMaxHit()) {
                    System.out.println("(crit)\n");
                } else if (char2.isAlive() && hit2 == 0) {
                    System.out.println("(anticrit)\n");
                } else {
                    System.out.println("\n");
                }
                char1.getHit(hit2);
            } else {
                System.out.printf("%s (%d) tried to hit %s (%d) (missed) \n\n", char2.getName(), char2.getHealth(), char1.getName(), char1.getHealth());
            }


        }

        if (char1.isDead()) {
            System.out.println(char1.getName() + " is dead");
        } else if (char2.isDead()) {
            System.out.println(char2.getName() + " is dead");
        }

    }
}
