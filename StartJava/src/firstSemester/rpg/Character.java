package firstSemester.rpg;

import java.util.concurrent.ThreadLocalRandom;

public class Character {
    private String name;
    private int level = 1;
    private int hp = 100;
    private int originalhp = 100;
    private int strength = 10;
    private int originalStrength = 10;
    private double crit = 0.25;
    private double originalCrit = 0.25;
    private double anticrit = 0.25;
    private int armor = 0;
    private int originalArmor = 0;
    private double agility = 0.25;
    Artifact arts[] = new Artifact[5];

    public Character(String name) {
        this.name = name;
    }

    public void info() {
        System.out.printf("name: %s \n" +
                "level: %d \n" +
                "hp: %d \n" +
                "strength: %d \n" +
                "crit: %f \n" +
                "armor: %d \n\n\n", name, level, hp, strength, crit, armor);
    }

    public int getStrength() {
        return strength;
    }

    public double getCrit() {
        return crit;
    }

    public int getArmor() {
        return armor;
    }

    public void setStrength(int i) {
        strength = i;
    }

    public String getName() {
        return name;
    }

    public void setLevel(int i) {
        level = i;
    }

    public int getHealth() {
        return hp;
    }

    public int hit() {
        if (isAlive()) {
            int min = (int) Math.round(getMinHit());
            int max = (int) Math.round(getMaxHit());
            int randomNum = ThreadLocalRandom.current().nextInt(min, max + 1);
            if (isCrit()) {
                return randomNum * 2;
            } else if (isAntiCrit()) {
                return 0;
            } else {
                return randomNum;
            }
        } else return 0;
    }

    public double getMinHit() {
        double i = strength * 0.5 + level;
        return i;
    }

    public double getMaxHit() {
        double i = strength + level;
        return i;
    }

    public boolean isAlive() {
        return hp > 0;
    }

    public boolean isDead() {
        return hp <= 0;
    }

    public boolean isAgility() {
        double f = Math.random();
        if (f <= agility) {
            return true;
        } else return false;
    }

    public boolean isCrit() {
        double f = Math.random();
        if (f <= crit) {
            return true;
        } else return false;
    }

    public boolean isAntiCrit() {
        double f = Math.random();
        if (f <= anticrit) {
            return true;
        } else return false;
    }

    public void getHit(int hit) {
        if ((hit - armor) > 0) {
            hp = hp - hit + armor;
        }
    }

    public void addArtifact(Artifact art) {
        switch (art.getType()) {
            case "weapon":
                arts[0] = art;
                break;
            case "helmet":
                arts[1] = art;
                break;
            case "chestplate":
                arts[2] = art;
                break;
            case "boots":
                arts[3] = art;
                break;
            case "shield":
                arts[4] = art;
                break;
            default:
                throw new RuntimeException("Wrong artifact type " + art);
        }
        recalcStats();
    }

    public void recalcStats() {
        hp = originalhp;
        strength = originalStrength;
        armor = originalArmor;
        crit = originalCrit;
        for (Artifact artifact : arts) {
            if (artifact != null) {
                hp = hp + artifact.getHp();
                strength = strength + artifact.getStrength();
                armor = armor + artifact.getArmor();
                crit = crit + artifact.getCrit();
            }
        }
    }


}
