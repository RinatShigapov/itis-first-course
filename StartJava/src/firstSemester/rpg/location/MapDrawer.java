package firstSemester.rpg.location;

public class MapDrawer {

    public static void drawMap(Map map) {
        char a = 97;
        for (int i = 0; i <= map.getSize(); i++) {
            for (int j = 0; j <= map.getSize(); j++) {
                if ((i == 0) && (j == 0)) {
                    System.out.print(" ");
                } else if (i == 0) {
                    System.out.print(" " + a);
                    a++;
                } else if (j == 0) {
                    System.out.print(i);
                } else {
                    System.out.print(" " + map.getCell(i - 1, j - 1));
                }
            }
            System.out.println();
        }
        System.out.println();

    }

}
