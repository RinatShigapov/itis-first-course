package firstSemester.rpg.location;

import java.util.Scanner;

import static firstSemester.rpg.location.MapDrawer.drawMap;

public class Element {

    private char element;
    private int x;
    private int y;
    private String move = " ";
    Map m;
    Scanner sc = new Scanner(System.in);

    public Element(char element, int x, int y) {
        this.element = element;
        this.x = x;
        this.y = y;
    }

    public void showElement(Map m) {
        m.setCell(x, y, element);
        drawMap(m);
    }

    public void moveElement(Map m) {
        System.out.println("w - up \n" +
                "s - down \n" +
                "d - right \n" +
                "a - left \n" +
                "exit - end");

        while (!(move.equals("exit"))) {
            try {
                move = sc.nextLine();
                if ((!(move.equals("exit")))) {
                    m.setCell(x, y, ' ');
                    switch (move) {
                        case "w":
                            x -= 1;
                            break;
                        case "s":
                            x += 1;
                            break;
                        case "a":
                            y -= 1;
                            break;
                        case "d":
                            y += 1;
                            break;
                        default:
                            System.out.println("This word didn't found.");
                    }
                    showElement(m);
                }

            } catch (ArrayIndexOutOfBoundsException exception) {
                if (y == m.getSize() + 1) {
                    y = m.getSize();
                } else if (y == 0) {
                    y = 1;
                } else if (x == m.getSize() + 1) {
                    x = m.getSize();
                } else if (x == 0) {
                    x = 1;
                }
                System.out.println("You can not go there, because you're at the end of the firstSemester.map.\n");
            }

        }
    }
}
