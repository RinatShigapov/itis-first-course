package firstSemester.rpg.location;

import static firstSemester.rpg.location.MapDrawer.drawMap;

public class LocationDemo {
    public static void main(String[] args) {
        Map demo = new Map(5);
        drawMap(demo);
        Element e = new Element('*', 3, 1);
        e.showElement(demo);
        e.moveElement(demo);
    }
}
