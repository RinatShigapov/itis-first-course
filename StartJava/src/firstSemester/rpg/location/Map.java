package firstSemester.rpg.location;

public class Map {
    private int size;
    private char[][] playMap;

    public Map(int size) {
        this.size = size;
        playMap = new char[size][size];
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                playMap[i][j] = ' ';
            }

        }
    }

    public char getCell(int x, int y) {
        return playMap[x][y];
    }

    public int getSize() {
        return size;
    }

    public void setCell(int x, int y, char c) {
        this.playMap[x - 1][y - 1] = c;
    }
}
