package firstSemester.rpg;

public class Bot extends Character {
    Artifact botArt;
    double chance = 1;

    public Bot(String name, Artifact botArt) {
        super(name);
        addArtifact(botArt);
    }

    public void dropArtifact(Artifact botArt){
        double f = Math.random();
        if (isDead() && f <= chance){
            System.out.printf("The artifact %s is dropped. \n" +
                    "Specifications: \n " +
                    "hp: %d \n" +
                    "strength: %d \n" +
                    "crit: %f \n" +
                    "armor: %d", botArt.getType(), botArt.getHp(), botArt.getStrength(), botArt.getCrit(), botArt.getArmor());
        }
    }



}
