package firstSemester.rpg;

public class Artifact {
    private String type;
    private int hp;
    private int strength;
    private double crit;
    private int armor;
    private String[] listTypes = {"weapon", "helmet", "chestplate", "boots", "shield"};

    public Artifact(String type, int hp, int strength, double crit, int armor) {
        this.type = type;
        this.hp = hp;
        this.strength = strength;
        this.crit = crit;
        this.armor = armor;

        boolean found = false;
        for (String type1: listTypes) {
            if(type.equals(type1)){
                found = true;
            }
        }

        if (!found){
            throw new RuntimeException("Wrong artifact type " + type);
        }

    }

    public String getType(){
        return type;
    }

    public int getHp() {
        return hp;
    }

    public int getStrength() {
        return strength;
    }

    public double getCrit() {
        return crit;
    }

    public int getArmor() {
        return armor;
    }




}
