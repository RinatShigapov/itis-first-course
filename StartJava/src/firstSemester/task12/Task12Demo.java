package firstSemester.task12;



public class Task12Demo {
    public static void main(String[] args) {
        Car car = new Car();
        GasStation gasStation = new GasStation();
        Plane plane = new Plane();
        Boat boat = new Boat();
        gasStation.fill(car);
        gasStation.fill(plane);
        gasStation.fill(boat);

        System.out.printf("Car: %d \n" +
                "Plane: %d \n" +
                "Boat: %d \n" , car.getFuel(), plane.getFuel(), boat.getFuel());

        Driver driver = new Driver();
        driver.drive(car, 500);
        System.out.println(car.getDistance());
    }
}
