package firstSemester.task12;

public class Plane implements Fuelable{
    private int fuel = 0;
    private int distance = 0;

    public int getDistance() {
        return distance;
    }

    public int getFuel() {
        return fuel;
    }

    @Override
    public void setFuel(int i) {
        fuel = i;
    }
}
