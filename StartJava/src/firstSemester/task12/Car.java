package firstSemester.task12;

public class Car implements Fuelable, Driveable {
    private int fuel = 0;
    private int distance = 0;

    public int getFuel() {
        return fuel;
    }

    public int getDistance() {
        return distance;
    }


    @Override
    public void setFuel(int i) {
        fuel = i;
    }


    @Override
    public void setDistance(int i) {
        distance = i;
    }
}
