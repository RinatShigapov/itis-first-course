package firstSemester.task3;

public class Task5 {
    public static void main(String[] args) {
        byte i = 0;
        boolean a = true;
        double d = 5.3;
        double f = 10.9;

        i++;
        System.out.println(i++ + " " + d*f);
        System.out.println(i);
        i--;
        System.out.println(i++);
        System.out.println((i == 1)? a : !a);
        System.out.println(i+d);
        System.out.println((int)(f-i));

    }
}
