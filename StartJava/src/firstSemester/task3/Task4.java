package firstSemester.task3;

public class Task4 {

    public static void main(String[] args) {


        int to = 50;
        int quantity = 7;
        int k = 1;

        for (int i = 0; i < (to / quantity) + 1; i++) {
            for (int j = 0; j < quantity; j++) {
                if (k <= to) {
                    if (k < 10) {
                        System.out.print("0" + k++ + " ");
                    } else {
                        System.out.print(k++ + " ");
                    }
                }
            }
            System.out.println();
        }
    }
}
