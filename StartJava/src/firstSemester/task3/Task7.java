package firstSemester.task3;

public class Task7 {
    static class Room {

        private double lengthRoom;
        private double widthRoom;
        private double areaRoom;

        public double getLengthRoom() {
            return lengthRoom;
        }

        public double getWidthRoom() {
            return widthRoom;
        }


        public Room(double lengthRoom, double widthRoom) {
            this.lengthRoom = lengthRoom;
            this.widthRoom = widthRoom;
        }

        public double getAreaRoom() {
            return lengthRoom * widthRoom;
        }

    }

    static class Table {


        private double lengthTab;
        private double widthTab;
        private double AreaTable;

        public double getLengthTab() {
            return lengthTab;
        }

        public double getAreaTable() {
            return ((lengthTab + lengthTab/4)*(widthTab + lengthTab/4));
        }

        public Table(double lengthTab, double widthTab) {
            this.lengthTab = lengthTab;
            this.widthTab = widthTab;
        }
    }



    public static void main(String[] args) {
        Table t = new Table(10,10);
       Room r = new Room(100, 100);
        System.out.println(t.getAreaTable());
        System.out.println(r.getAreaRoom());
        double newAreaRoom = (r.lengthRoom - t.lengthTab/4) * (r.widthRoom - t.lengthTab/4);
        System.out.println(newAreaRoom);
        int quantity = (int)(newAreaRoom/t.getAreaTable());
        System.out.println(quantity);
    }

}
