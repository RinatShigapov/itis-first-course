package firstSemester.task5;

public class StringTest {
    public static void main(String[] args) {

        String name = "John Smith";
        String lastName = name.substring(name.indexOf(' ') + 1);
        lastName = lastName.toUpperCase();
        System.out.println(lastName + ".");

        String a = "200 mm";
        a = a.substring(0, a.indexOf(' '));
        int b = Integer.parseInt(a);
        b = b / 10;
        System.out.println(b + "cm");
    }
}
