package firstSemester.task5;

public class Converter {
    public static void main(String[] args) {
        String a = "200 mm = ";
        String b = a.substring(0, a.indexOf(' '));
        double d = Double.parseDouble(b);
        System.out.println(a + d / 10 + " cm \n" + a + d / 100 + " m \n" + a + d / 25.4 + " in");
    }
}
