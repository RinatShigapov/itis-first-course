package firstSemester.task8;

import java.lang.annotation.*;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)

public @interface Sizes {
    public int width();
    public int height();
    public int length();

}
