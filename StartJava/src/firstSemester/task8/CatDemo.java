package firstSemester.task8;

public class CatDemo {
    public static void main(String[] args) {
        Cat c = new Cat();
        Class cl1 = c.getClass();
        Massa mas = (Massa)cl1.getAnnotation(Massa.class);

        System.out.println("Cat massa: " + mas.value());
        c.say();
        c.soundGood();

        Desk d = new Desk();
        Class cl = d.getClass();
        Color col = (Color)cl.getAnnotation(Color.class);
        Sizes siz = (Sizes)cl.getAnnotation(Sizes.class);

        System.out.println("Desk color is: " + col.value() + "\n Height: " + siz.height() + "\n Length: " + siz.length() + "\n Width: " + siz.width());

    }
}
