package firstSemester.task2;

public class Rectangle {

    private int width = 0;
    private int height = 0;
    private Point origin;

    public void setOrigin(Point origin) {
        this.origin = origin;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public Point getOrigin() {
        return origin;
    }

    public Rectangle() {
        origin = new Point(0,0);
    }

    public Rectangle(Point p){
        origin = p;
    }

    public Rectangle(Point p, int w, int h) {
        width = w;
        height = h;
        origin = new Point(0,0);
    }

    public Rectangle(int w, int h) {
        width = w;
        height = h;
    }

    public int getArea() {
        return height*width;
    }

    public void move(int x, int y){
        origin.setX(x);{
            x=x;
        }
        origin.setY(y);{
            y=y;
        }

    }
}
