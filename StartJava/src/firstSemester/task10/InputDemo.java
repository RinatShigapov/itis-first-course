package firstSemester.task10;

import java.util.Scanner;

public class InputDemo {

    public static void main(String[] args) {
        /*String ch;
        boolean run = true;
        Scanner in = new Scanner(System.in);
        while (run) {
            ch = in.nextLine();
            run = !ch.equals("q");
            if (run) {
                System.out.println("Input: " + ch.toUpperCase());
            }
        }
        System.out.println("Finished");*/

        String name;
        int age;
        Scanner in = new Scanner(System.in);


        System.out.println("What is your name?");
        name = in.nextLine();
        System.out.println("How old are you?");
        age = Integer.parseInt(in.nextLine());
        System.out.println("Hi, " + name + "!");
        String message = "";

        while (!(message.equals("bye"))) {
            message = in.nextLine();
            if (message.equals("hello")) {
                System.out.println("How are you?");
                message = in.nextLine();
                if (message.equals("ok")) {
                    message = in.nextLine();
                    System.out.println("It's so good");
                } else if (message.equals("bad")) {
                    System.out.println("Oh, I'm sorry");
                }
            }
            if (message.equals("What is my name?")) {
                System.out.println(name);
            }
            if (message.equals("How old am I?")) {
                System.out.println(age);
            }
            if (message.equals("What do you know about me?")) {
                System.out.println("You name is " + name + ". You are " + age + " years old.");
            }
        }
        System.out.println("Bye");
    }
}
