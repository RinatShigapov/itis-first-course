package firstSemester.task10;

import java.util.Scanner;

public class TestDemo {
    public static void main(String[] args) {
        test();

    }

    public static void test() {
        Scanner sc = new Scanner(System.in);
        String rightAnswer = "";
        int choice;
        int result = 0;
        int right = 0;


        String[] questions = {"What is capital of Great Britain?",
                "What is capital of Russia?",
                "What is capital of USA?",
                "What is capital of Germany?",
                "What is capital of Italy?"
        };

        String[] answers = {"Moscow", "*London", "Washington",
                "*Moscow", "London", "Washington",
                "Moscow", "London", "*Washington",
                "Paris", "*Berlin", "Washington",
                "*Rome", "London", "Paris",
        };

        for (int i = 0; i < questions.length; i++) {
            System.out.println(questions[i]);
            for (int k = 0; k < 3; k++) {
                if (answers[3 * i + k].substring(0, 1).equals("*")) {
                    System.out.println(k + 1 + "." + answers[3 * i + k].substring(1));
                    rightAnswer = answers[3 * i + k].substring(1);
                    right = k + 1;
                } else System.out.println(k + 1 + "." + answers[3 * i + k]);
            }
            choice = sc.nextInt();
            if (choice == right) {
                result++;
            }
        }

        System.out.println("Your result: " + result + "/" + questions.length);
    }

}

