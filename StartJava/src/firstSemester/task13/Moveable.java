package firstSemester.task13;

public interface Moveable {
    public void moveTo(int x, int y);
}
