package firstSemester.task13;

public class Task13Demo {
    public static void main(String[] args) {
        Point p = new Point();
        p.setCoords(5,7);
        p.setColor("red");
        p.print();
        p.moveTo(3,3);
        System.out.println(p.getX() + " " + p.getY());

        Apple app = new Apple();
        app.setCoords(4,2);
        app.print();
        app.moveTo(3,5);
        System.out.println(app.getX() + " " + app.getY());
        app.setColor("green");
        app.print();
    }
}
