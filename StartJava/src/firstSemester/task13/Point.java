package firstSemester.task13;

public class Point implements Moveable, Placeable, Printable, Colorable {
    private int x;
    private int y;
    private String color = new String("hasn't color");

    @Override
    public void moveTo(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public int getX() {
        return x;
    }

    @Override
    public int getY() {
        return y;
    }

    @Override
    public void setCoords(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public void print() {
        System.out.printf("x = %d %n" +
                "y = %d %n" +
                "color: %s %n", x, y, color);
    }

    @Override
    public void setColor(String s) {
        color = s;
    }
}
