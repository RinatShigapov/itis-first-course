package firstSemester.task13;

public interface Placeable {
    public void setCoords(int x, int y);
    public int getX();
    public int getY();
}
