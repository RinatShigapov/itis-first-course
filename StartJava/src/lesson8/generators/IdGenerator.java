package lesson8.generators;

public interface IdGenerator {
    Long getNext();
}
