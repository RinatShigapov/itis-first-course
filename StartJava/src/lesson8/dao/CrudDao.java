package lesson8.dao;

public interface CrudDao<T> {
    //Create
    T save(T models);

    //Read
    T find(Long id);

    //Update
    void update(T models);

    //Delete
    void delete(Long id);
}
