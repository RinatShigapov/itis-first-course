package lesson8.dao;

import lesson8.models.User;

public interface UserDao extends CrudDao<User> {
}
