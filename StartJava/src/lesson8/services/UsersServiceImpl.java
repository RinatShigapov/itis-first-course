package lesson8.services;

import lesson8.dao.UserDao;
import lesson8.dao.UserDaoTextFileImpl;
import lesson8.models.User;

public class UsersServiceImpl implements UserService{
    private UserDao userDao;

    public UsersServiceImpl() {
        this.userDao = new UserDaoTextFileImpl();
    }

    @Override
    public User signUp(User user) {
        if(user.getPassword().length() < 8){
            throw new IllegalArgumentException("Password should not be shorter than 8 symbols");
        }
        return userDao.save(user);
    }

    @Override
    public void signIp(User user) {

    }
}
