package lesson8;

import lesson8.models.User;
import lesson8.services.UserService;
import lesson8.services.UsersServiceImpl;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Name");
        String username = sc.nextLine();
        System.out.println("Password");
        String password = sc.nextLine();
        User u = new User(username, password);
        UserService service = new UsersServiceImpl();
        User createdUser = service.signUp(u);
        System.out.println(createdUser);
    }
}
