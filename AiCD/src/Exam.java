import java.util.Scanner;

public class Exam {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int m = sc.nextInt();
        int res = rec(n,m);
        System.out.println(res);
    }

    public static int rec(int n, int m) {
        int[] arr = new int[n+4];
        arr[0] = 0;
        arr[1] = 0;
        arr[2] = 0;
        arr[3] = 1;
        for (int i = 4; i < n + 3; i++) {
            arr[i] = (arr[i-1] + arr[i-2] + arr[i-3] + arr[i-4]) % m;
        }
        return arr[n+2];
    }
}
